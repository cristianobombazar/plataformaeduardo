package br.com.sys.dto;

import java.math.BigDecimal;

public class RelatorioProdutoVendidoDTO {
	
	private Integer    produtoID;
	private String     produtoDescricao;
	private BigDecimal quantidade;
	
	public RelatorioProdutoVendidoDTO() {
	}

	public Integer getProdutoID() {
		return produtoID;
	}

	public void setProdutoID(Integer produtoID) {
		this.produtoID = produtoID;
	}

	public String getProdutoDescricao() {
		return produtoDescricao;
	}

	public void setProdutoDescricao(String produtoDescricao) {
		this.produtoDescricao = produtoDescricao;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

}
