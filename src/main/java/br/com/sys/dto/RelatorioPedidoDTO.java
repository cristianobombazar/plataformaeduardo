package br.com.sys.dto;

import java.math.BigDecimal;
import java.util.Date;

public class RelatorioPedidoDTO {
	
	private Integer    pedidoID;
	private Date       pedidoData;
	private BigDecimal pedidoTotal;
	private String     pedidoStatus;
	private String     clienteNome;
	
	public RelatorioPedidoDTO() {
	}

	public Integer getPedidoID() {
		return pedidoID;
	}

	public void setPedidoID(Integer pedidoID) {
		this.pedidoID = pedidoID;
	}

	public Date getPedidoData() {
		return pedidoData;
	}

	public void setPedidoData(Date pedidoData) {
		this.pedidoData = pedidoData;
	}

	public BigDecimal getPedidoTotal() {
		return pedidoTotal;
	}

	public void setPedidoTotal(BigDecimal pedidoTotal) {
		this.pedidoTotal = pedidoTotal;
	}

	public String getPedidoStatus() {
		return pedidoStatus;
	}

	public void setPedidoStatus(String pedidoStatus) {
		this.pedidoStatus = pedidoStatus;
	}

	public String getClienteNome() {
		return clienteNome;
	}

	public void setClienteNome(String clienteNome) {
		this.clienteNome = clienteNome;
	}
	

}
