package br.com.sys.dto.filtro;

import java.util.Date;

import br.com.sys.model.cadastro.Cliente;
import br.com.sys.model.cadastro.Produto;
import br.com.sys.model.cadastro.Supermercado;

public class Filtro {

	private Date dataInicial;
	private Date dataFinal;
	private Integer status;
	
	private Cliente cliente;
	private Produto produto;
	
	private Supermercado supermercado;
	
	public Filtro() {
	}
	public void setDataInicial(Date dataInicial) {
		this.dataInicial = dataInicial;
	}
	public Date getDataInicial() {
		return dataInicial;
	}
	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}
	public Date getDataFinal() {
		return dataFinal;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Integer getStatus() {
		return status;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	public Supermercado getSupermercado() {
		return supermercado;
	}
	
}
