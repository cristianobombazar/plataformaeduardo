package br.com.sys.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sys.model.cadastro.FormaPagamento;
import br.com.sys.model.cadastro.Supermercado;

@Repository
public interface FormaPagamentoRepository extends JpaRepository<FormaPagamento, Integer> {
	
	@Query("SELECT forma FROM FormaPagamento forma WHERE forma.supermercado = ?1 AND forma.indicadorUso = "+FormaPagamento.INDICADOR_USO_ATIVO)
	List<FormaPagamento> findAll(Supermercado supermercado) throws Exception;

}
