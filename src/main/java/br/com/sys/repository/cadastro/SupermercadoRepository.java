package br.com.sys.repository.cadastro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.sys.model.cadastro.Supermercado;

@Repository
public interface SupermercadoRepository extends JpaRepository<Supermercado, Integer>{

}
