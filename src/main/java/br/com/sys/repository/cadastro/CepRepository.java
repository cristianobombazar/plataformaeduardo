package br.com.sys.repository.cadastro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.sys.model.cadastro.Cep;

@Repository
public interface CepRepository extends JpaRepository<Cep, Integer> {

}
