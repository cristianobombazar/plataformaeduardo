package br.com.sys.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sys.model.cadastro.Cliente;
import br.com.sys.model.cadastro.Supermercado;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	
	@Query("SELECT cliente FROM Cliente cliente WHERE cliente.supermercado = ?1")
	List<Cliente> findAll(Supermercado supermercado) throws Exception;

}
