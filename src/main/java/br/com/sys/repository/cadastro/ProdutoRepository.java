package br.com.sys.repository.cadastro;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import br.com.sys.model.cadastro.Produto;
import br.com.sys.model.cadastro.Supermercado;

@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer> {
	
	@Query("SELECT produto FROM Produto produto WHERE produto.supermercado = ?1")
	List<Produto> findAll(Supermercado supermercado) throws Exception;
	
}
