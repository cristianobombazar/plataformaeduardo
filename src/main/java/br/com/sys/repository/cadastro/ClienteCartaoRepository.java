package br.com.sys.repository.cadastro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sys.model.cadastro.ClienteCartao;

@Repository
public interface ClienteCartaoRepository extends JpaRepository<ClienteCartao, Integer> {
	
	@Query(value = "DELETE FROM cadastro.cartao_cliente WHERE id = ?1", nativeQuery = true)
	@Modifying
	void deletarCartao(Integer id) throws Exception;

}
