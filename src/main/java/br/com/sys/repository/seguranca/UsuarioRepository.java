package br.com.sys.repository.seguranca;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sys.model.seguranca.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	@Query("FROM Usuario WHERE login = ?1 OR email = ?2")
	public Usuario getUsuario(String login, String email);
}
