package br.com.sys.repository.faturamento;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.model.faturamento.Pedido;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Integer> {

	@Query("SELECT pedido FROM Pedido pedido WHERE pedido.supermercado = ?1")
	List<Pedido> findAll(Supermercado supermercado) throws Exception;
	
	@Query(value = "DELETE FROM faturamento.pedido_item WHERE id = ?1", nativeQuery = true)
	@Modifying
	void deletarItem(Integer id) throws Exception;
	
}
