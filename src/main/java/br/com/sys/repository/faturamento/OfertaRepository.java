package br.com.sys.repository.faturamento;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.model.faturamento.Oferta;

@Repository
public interface OfertaRepository extends JpaRepository<Oferta, Integer> {

	@Query("SELECT oferta FROM Oferta oferta WHERE oferta.supermercado = ?1")
	List<Oferta> findAll(Supermercado supermercado) throws Exception;
	
	@Query(value = "DELETE FROM faturamento.oferta_produto WHERE id = ?1", nativeQuery = true)
	@Modifying
	void deletarItem(Integer id) throws Exception;
	
}
