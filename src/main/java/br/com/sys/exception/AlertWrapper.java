package br.com.sys.exception;

public class AlertWrapper {
	private final String tipo;
	private final Throwable throwable;
	private String titulo;
	private String mensagem;

	public static final String TIPO_SUCCESS = "success";
	public static final String TIPO_INFO = "info";
	public static final String TIPO_WARNING = "warning";
	public static final String TIPO_DANGER = "danger";

	public AlertWrapper(Throwable throwable) {
		this(TIPO_INFO, throwable);
	}

	public AlertWrapper(String tipo, Throwable throwable) {
		this(tipo, throwable.getMessage(), throwable);
	}

	public AlertWrapper(String tipo, String mensagem, Throwable throwable) {
		this(tipo, "", mensagem, throwable);
	}

	public AlertWrapper(String tipo, String titulo, String mensagem, Throwable throwable) {
		this.tipo = tipo;
		this.titulo = titulo;
		if (mensagem == null) {
			mensagem = throwable.getClass().getName();
		}
		this.mensagem = mensagem;
		this.throwable = throwable;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getTipo() {
		return tipo;
	}

	public Throwable getThrowable() {
		return throwable;
	}

}
