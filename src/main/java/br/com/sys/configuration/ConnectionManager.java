package br.com.sys.configuration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

public class ConnectionManager {
	private static final ConnectionManager instance = new ConnectionManager();

	private static DataSource dataSource;
	private static int versaoDb;

	private ConnectionManager() {
	}

	public static ConnectionManager getInstance() {
		return instance;
	}

	public Connection openConnection() throws SQLException {
		return dataSource.getConnection();
	}

	public static void close(Connection conn) {
		try {
			conn.close();
		} catch (Exception e) {
		}
	}

	public static void close(PreparedStatement ps, ResultSet rs) {
		try {
			rs.close();
		} catch (Exception e) {
		}
		try {
			ps.close();
		} catch (Exception e) {
		}
	}

	public static void close(Connection conn, PreparedStatement ps, ResultSet rs) {
		close(ps, rs);
		close(conn);
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		ConnectionManager.dataSource = dataSource;
	}

	public int getVersaoDb() {
		return versaoDb;
	}

	public void setVersaoDb(int versaoDb) {
		ConnectionManager.versaoDb = versaoDb;
	}
}
