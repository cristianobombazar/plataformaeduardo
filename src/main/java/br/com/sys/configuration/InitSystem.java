package br.com.sys.configuration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InitSystem {
	
	public static void init() {
		Connection conn = null;
		try {
			conn = ConnectionManager.getInstance().openConnection();
			initSequence(conn);
		} catch (SQLException e) {
			return;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM faturamento.pedido_item; ");
		sql.append("DELETE FROM faturamento.pedido; ");
		sql.append("DELETE FROM faturamento.oferta_produto; ");
		sql.append("DELETE FROM faturamento.oferta; ");		
		sql.append("DELETE FROM cadastro.cartao_cliente; ");
		sql.append("DELETE FROM cadastro.cliente_endereco; ");
		sql.append("DELETE FROM cadastro.cliente; ");
		sql.append("DELETE FROM cadastro.forma_pagamento; ");
		sql.append("DELETE FROM seguranca.usuario_autoridade; ");
		sql.append("DELETE FROM seguranca.usuario; ");
		sql.append("DELETE FROM cadastro.produto; ");	
		sql.append("DELETE FROM cadastro.supermercado_endereco; ");
		sql.append("DELETE FROM cadastro.supermercado; ");
		sql.append("DELETE FROM cadastro.municipio; ");
		sql.append("DELETE FROM cadastro.cep; ");
		sql.append("DELETE FROM cadastro.bairro; ");	
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.bairro(id, nome) VALUES (nextval('cadastro.bairro_id_seq'), 'CENTRO'); ");
		sql.append("INSERT INTO cadastro.bairro(id, nome) VALUES (nextval('cadastro.bairro_id_seq'), 'CENTRO 2'); ");
		sql.append("INSERT INTO cadastro.bairro(id, nome) VALUES (nextval('cadastro.bairro_id_seq'), 'CENTRO 3'); ");
		sql.append("INSERT INTO cadastro.bairro(id, nome) VALUES (nextval('cadastro.bairro_id_seq'), 'CENTRO 4'); ");
		sql.append("INSERT INTO cadastro.bairro(id, nome) VALUES (nextval('cadastro.bairro_id_seq'), 'CENTRO 5'); ");
		
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.cep(id, codigo) VALUES (nextval('cadastro.cep_id_seq'), 88750000); ");
		sql.append("INSERT INTO cadastro.cep(id, codigo) VALUES (nextval('cadastro.cep_id_seq'), 98750000); ");
		sql.append("INSERT INTO cadastro.cep(id, codigo) VALUES (nextval('cadastro.cep_id_seq'), 99750000); ");
		sql.append("INSERT INTO cadastro.cep(id, codigo) VALUES (nextval('cadastro.cep_id_seq'), 99750001); ");
		sql.append("INSERT INTO cadastro.cep(id, codigo) VALUES (nextval('cadastro.cep_id_seq'), 99750002); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.municipio(id, nome, uf) VALUES (nextval('cadastro.municipio_id_seq'), 'BRA�O DO NORTE', 'SC'); ");
		sql.append("INSERT INTO cadastro.municipio(id, nome, uf) VALUES (nextval('cadastro.municipio_id_seq'), 'BRA�O DO NORTE 2', 'SC'); ");
		sql.append("INSERT INTO cadastro.municipio(id, nome, uf) VALUES (nextval('cadastro.municipio_id_seq'), 'BRA�O DO NORTE 3', 'SC'); ");
		sql.append("INSERT INTO cadastro.municipio(id, nome, uf) VALUES (nextval('cadastro.municipio_id_seq'), 'BRA�O DO NORTE 4', 'SC'); ");
		sql.append("INSERT INTO cadastro.municipio(id, nome, uf) VALUES (nextval('cadastro.municipio_id_seq'), 'BRA�O DO NORTE 5', 'SC'); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.supermercado(id, cnpj, indicadoruso, logo, razaosocial) VALUES (nextval('cadastro.supermercado_id_seq'), '06494495923', 1, null, 'SUPERMERCADO EDUARDO'); ");
		sql.append("INSERT INTO cadastro.supermercado(id, cnpj, indicadoruso, logo, razaosocial) VALUES (nextval('cadastro.supermercado_id_seq'), '06494495924', 1, null, 'SUPERMERCADO EDUARDO 2'); ");
		sql.append("INSERT INTO cadastro.supermercado(id, cnpj, indicadoruso, logo, razaosocial) VALUES (nextval('cadastro.supermercado_id_seq'), '06494495925', 1, null, 'SUPERMERCADO EDUARDO 3'); ");
		sql.append("INSERT INTO cadastro.supermercado(id, cnpj, indicadoruso, logo, razaosocial) VALUES (nextval('cadastro.supermercado_id_seq'), '06494495926', 1, null, 'SUPERMERCADO EDUARDO 4'); ");
		sql.append("INSERT INTO cadastro.supermercado(id, cnpj, indicadoruso, logo, razaosocial) VALUES (nextval('cadastro.supermercado_id_seq'), '06494495927', 1, null, 'SUPERMERCADO EDUARDO 5'); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.supermercado_endereco(id, complemento, numero, rua, bairro_id, cep_id, municipio_id, supermercado_id) VALUES (nextval('cadastro.supermercado_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL', 1, 1, 1, 1); ");
		sql.append("INSERT INTO cadastro.supermercado_endereco(id, complemento, numero, rua, bairro_id, cep_id, municipio_id, supermercado_id) VALUES (nextval('cadastro.supermercado_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL 2', 2, 2, 2, 2); ");
		sql.append("INSERT INTO cadastro.supermercado_endereco(id, complemento, numero, rua, bairro_id, cep_id, municipio_id, supermercado_id) VALUES (nextval('cadastro.supermercado_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL 3', 3, 3, 3, 3); ");
		sql.append("INSERT INTO cadastro.supermercado_endereco(id, complemento, numero, rua, bairro_id, cep_id, municipio_id, supermercado_id) VALUES (nextval('cadastro.supermercado_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL 4', 4, 4, 4, 4); ");
		sql.append("INSERT INTO cadastro.supermercado_endereco(id, complemento, numero, rua, bairro_id, cep_id, municipio_id, supermercado_id) VALUES (nextval('cadastro.supermercado_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL 5', 5, 5, 5, 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.forma_pagamento(id, descricao, indicadoruso, supermercado_id, indicadorcartaocredito) VALUES (nextval('cadastro.forma_pagamento_id_seq'), '� VISTA', 1, 1, 1); ");
		sql.append("INSERT INTO cadastro.forma_pagamento(id, descricao, indicadoruso, supermercado_id, indicadorcartaocredito) VALUES (nextval('cadastro.forma_pagamento_id_seq'), '2X', 1, 2, 1); ");
		sql.append("INSERT INTO cadastro.forma_pagamento(id, descricao, indicadoruso, supermercado_id, indicadorcartaocredito) VALUES (nextval('cadastro.forma_pagamento_id_seq'), '3X', 1, 3, 1); ");
		sql.append("INSERT INTO cadastro.forma_pagamento(id, descricao, indicadoruso, supermercado_id, indicadorcartaocredito) VALUES (nextval('cadastro.forma_pagamento_id_seq'), '4X', 1, 4, 1); ");
		sql.append("INSERT INTO cadastro.forma_pagamento(id, descricao, indicadoruso, supermercado_id, indicadorcartaocredito) VALUES (nextval('cadastro.forma_pagamento_id_seq'), '5X', 1, 5, 1); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.produto(id, descricao, generatedvalueqrcodeencoder, marca, obs, qrcode, quantidade, valor, supermercado_id) VALUES (nextval('cadastro.produto_id_seq'), 'PRODUTO 1', 1, 'MARCA 1', null, 'lol1', 1.00, 1.00, 1); ");
		sql.append("INSERT INTO cadastro.produto(id, descricao, generatedvalueqrcodeencoder, marca, obs, qrcode, quantidade, valor, supermercado_id) VALUES (nextval('cadastro.produto_id_seq'), 'PRODUTO 2', 1, 'MARCA 2', null, 'lol2', 2.00, 2.00, 2); ");
		sql.append("INSERT INTO cadastro.produto(id, descricao, generatedvalueqrcodeencoder, marca, obs, qrcode, quantidade, valor, supermercado_id) VALUES (nextval('cadastro.produto_id_seq'), 'PRODUTO 3', 1, 'MARCA 3', null, 'lol3', 3.00, 3.00, 3); ");
		sql.append("INSERT INTO cadastro.produto(id, descricao, generatedvalueqrcodeencoder, marca, obs, qrcode, quantidade, valor, supermercado_id) VALUES (nextval('cadastro.produto_id_seq'), 'PRODUTO 4', 1, 'MARCA 4', null, 'lol4', 4.00, 4.00, 4); ");
		sql.append("INSERT INTO cadastro.produto(id, descricao, generatedvalueqrcodeencoder, marca, obs, qrcode, quantidade, valor, supermercado_id) VALUES (nextval('cadastro.produto_id_seq'), 'PRODUTO 5', 1, 'MARCA 5', null, 'lol5', 5.00, 5.00, 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.cliente(id, cpfcnpj, email, indicadoruso, nome, rg, telefone, tipo, supermercado_id) VALUES (nextval('cadastro.cliente_id_seq'), '06494495923', 'EDUARDO@bgstudio.com.br', 1, 'EDUARDO YUCHO 1', '5728104', '(48) 9 96779763', 1, 1); ");
		sql.append("INSERT INTO cadastro.cliente(id, cpfcnpj, email, indicadoruso, nome, rg, telefone, tipo, supermercado_id) VALUES (nextval('cadastro.cliente_id_seq'), '06494495922', 'EDUARDO@bgstudio.com.br', 1, 'EDUARDO YUCHO 2', '5728104', '(48) 9 96779763', 1, 2); ");
		sql.append("INSERT INTO cadastro.cliente(id, cpfcnpj, email, indicadoruso, nome, rg, telefone, tipo, supermercado_id) VALUES (nextval('cadastro.cliente_id_seq'), '06494495923', 'EDUARDO@bgstudio.com.br', 1, 'EDUARDO YUCHO 3', '5728104', '(48) 9 96779763', 1, 3); ");
		sql.append("INSERT INTO cadastro.cliente(id, cpfcnpj, email, indicadoruso, nome, rg, telefone, tipo, supermercado_id) VALUES (nextval('cadastro.cliente_id_seq'), '06494495924', 'EDUARDO@bgstudio.com.br', 1, 'EDUARDO YUCHO 4', '5728104', '(48) 9 96779763', 1, 4); ");
		sql.append("INSERT INTO cadastro.cliente(id, cpfcnpj, email, indicadoruso, nome, rg, telefone, tipo, supermercado_id) VALUES (nextval('cadastro.cliente_id_seq'), '06494495925', 'EDUARDO@bgstudio.com.br', 1, 'EDUARDO YUCHO 5', '5728104', '(48) 9 96779763', 1, 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.cliente_endereco(id, complemento, numero, rua, bairro_id, cep_id, cliente_id, municipio_id) VALUES (nextval('cadastro.cliente_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL'  , 1, 1, 1, 1); ");
		sql.append("INSERT INTO cadastro.cliente_endereco(id, complemento, numero, rua, bairro_id, cep_id, cliente_id, municipio_id) VALUES (nextval('cadastro.cliente_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL 2', 2, 2, 2, 2); ");
		sql.append("INSERT INTO cadastro.cliente_endereco(id, complemento, numero, rua, bairro_id, cep_id, cliente_id, municipio_id) VALUES (nextval('cadastro.cliente_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL 3', 3, 3, 3, 3); ");
		sql.append("INSERT INTO cadastro.cliente_endereco(id, complemento, numero, rua, bairro_id, cep_id, cliente_id, municipio_id) VALUES (nextval('cadastro.cliente_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL 4', 4, 4, 4, 4); ");
		sql.append("INSERT INTO cadastro.cliente_endereco(id, complemento, numero, rua, bairro_id, cep_id, cliente_id, municipio_id) VALUES (nextval('cadastro.cliente_endereco_id_seq'), 'CASA', 'S/N', 'ESTRADA GERAL 5', 5, 5, 5, 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO cadastro.cartao_cliente(id, bandeira, codigoseguranca, dataexpiracao, nomeimpresso, numerocartao, tipo, cliente_id) VALUES (nextval('cadastro.cliente_cartao_id_seq'), 1, 019, '2018-01-01', 'EDUARDO YUCHO 1', '01', 1, 1); ");
		sql.append("INSERT INTO cadastro.cartao_cliente(id, bandeira, codigoseguranca, dataexpiracao, nomeimpresso, numerocartao, tipo, cliente_id) VALUES (nextval('cadastro.cliente_cartao_id_seq'), 2, 029, '2018-02-01', 'EDUARDO YUCHO 2', '02', 1, 2); ");
		sql.append("INSERT INTO cadastro.cartao_cliente(id, bandeira, codigoseguranca, dataexpiracao, nomeimpresso, numerocartao, tipo, cliente_id) VALUES (nextval('cadastro.cliente_cartao_id_seq'), 3, 039, '2018-03-01', 'EDUARDO YUCHO 3', '03', 1, 3); ");
		sql.append("INSERT INTO cadastro.cartao_cliente(id, bandeira, codigoseguranca, dataexpiracao, nomeimpresso, numerocartao, tipo, cliente_id) VALUES (nextval('cadastro.cliente_cartao_id_seq'), 4, 049, '2018-04-01', 'EDUARDO YUCHO 4', '04', 1, 4); ");
		sql.append("INSERT INTO cadastro.cartao_cliente(id, bandeira, codigoseguranca, dataexpiracao, nomeimpresso, numerocartao, tipo, cliente_id) VALUES (nextval('cadastro.cliente_cartao_id_seq'), 5, 059, '2018-05-01', 'EDUARDO YUCHO 5', '05', 1, 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO faturamento.oferta( id, descricao, datacadastro, datafim, datainicio, obs, supermercado_id) VALUES (nextval('faturamento.oferta_id_seq'), 'OFERTA 1', '2011-10-01', '2019-10-01', '2011-11-01', null, 1); ");
		sql.append("INSERT INTO faturamento.oferta( id, descricao, datacadastro, datafim, datainicio, obs, supermercado_id) VALUES (nextval('faturamento.oferta_id_seq'), 'OFERTA 2', '2011-10-02', '2019-10-02', '2011-11-02', null, 2); ");
		sql.append("INSERT INTO faturamento.oferta( id, descricao, datacadastro, datafim, datainicio, obs, supermercado_id) VALUES (nextval('faturamento.oferta_id_seq'), 'OFERTA 3', '2011-10-03', '2019-10-03', '2011-11-03', null, 3); ");
		sql.append("INSERT INTO faturamento.oferta( id, descricao, datacadastro, datafim, datainicio, obs, supermercado_id) VALUES (nextval('faturamento.oferta_id_seq'), 'OFERTA 4', '2011-10-04', '2019-10-04', '2011-11-04', null, 4); ");
		sql.append("INSERT INTO faturamento.oferta( id, descricao, datacadastro, datafim, datainicio, obs, supermercado_id) VALUES (nextval('faturamento.oferta_id_seq'), 'OFERTA 5', '2011-10-05', '2019-10-05', '2011-11-05', null, 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO faturamento.oferta_produto( id, quantidade, quantidadedisponivel, valor, oferta_id, produto_id) VALUES (nextval('faturamento.oferta_produto_id_seq'), 1.00, 1.00, 1.00, 1, 1); ");
		sql.append("INSERT INTO faturamento.oferta_produto( id, quantidade, quantidadedisponivel, valor, oferta_id, produto_id) VALUES (nextval('faturamento.oferta_produto_id_seq'), 2.00, 2.00, 2.00, 2, 2); ");
		sql.append("INSERT INTO faturamento.oferta_produto( id, quantidade, quantidadedisponivel, valor, oferta_id, produto_id) VALUES (nextval('faturamento.oferta_produto_id_seq'), 3.00, 3.00, 3.00, 3, 3); ");
		sql.append("INSERT INTO faturamento.oferta_produto( id, quantidade, quantidadedisponivel, valor, oferta_id, produto_id) VALUES (nextval('faturamento.oferta_produto_id_seq'), 4.00, 4.00, 4.00, 4, 4); ");
		sql.append("INSERT INTO faturamento.oferta_produto( id, quantidade, quantidadedisponivel, valor, oferta_id, produto_id) VALUES (nextval('faturamento.oferta_produto_id_seq'), 5.00, 5.00, 5.00, 5, 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO faturamento.pedido(id, data, obs, situacao, cliente_id, supermercado_id, formapagamento_id, oferta_id) VALUES (nextval('faturamento.pedido_id_seq'), '2017-10-11', null, 1, 1, 1, 1, 1); ");
		sql.append("INSERT INTO faturamento.pedido(id, data, obs, situacao, cliente_id, supermercado_id, formapagamento_id, oferta_id) VALUES (nextval('faturamento.pedido_id_seq'), '2017-10-12', null, 1, 2, 2, 2, 2); ");
		sql.append("INSERT INTO faturamento.pedido(id, data, obs, situacao, cliente_id, supermercado_id, formapagamento_id, oferta_id) VALUES (nextval('faturamento.pedido_id_seq'), '2017-10-13', null, 1, 3, 3, 3, 3); ");
		sql.append("INSERT INTO faturamento.pedido(id, data, obs, situacao, cliente_id, supermercado_id, formapagamento_id, oferta_id) VALUES (nextval('faturamento.pedido_id_seq'), '2017-10-14', null, 1, 4, 4, 4, 4); ");
		sql.append("INSERT INTO faturamento.pedido(id, data, obs, situacao, cliente_id, supermercado_id, formapagamento_id, oferta_id) VALUES (nextval('faturamento.pedido_id_seq'), '2017-10-15', null, 1, 5, 5, 5, 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO faturamento.pedido_item(id, quantidade, valor,  pedido_id, produto_id) VALUES (nextval('faturamento.pedido_item_id_seq'), 1.00, 1.00,  1, 1); ");
		sql.append("INSERT INTO faturamento.pedido_item(id, quantidade, valor,  pedido_id, produto_id) VALUES (nextval('faturamento.pedido_item_id_seq'), 2.00, 2.00,  2, 2); ");
		sql.append("INSERT INTO faturamento.pedido_item(id, quantidade, valor,  pedido_id, produto_id) VALUES (nextval('faturamento.pedido_item_id_seq'), 3.00, 3.00,  3, 3); ");
		sql.append("INSERT INTO faturamento.pedido_item(id, quantidade, valor,  pedido_id, produto_id) VALUES (nextval('faturamento.pedido_item_id_seq'), 4.00, 4.00,  4, 4); ");
		sql.append("INSERT INTO faturamento.pedido_item(id, quantidade, valor,  pedido_id, produto_id) VALUES (nextval('faturamento.pedido_item_id_seq'), 5.00, 5.00,  5, 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO seguranca.usuario(id, email, login, nome, senha, ultimaalteracao, supermercado_id) VALUES (1, 'EDUARDO.YUCHO@outlook.com', 'sys' , 'EDUARDO YUCHO', '$2a$10$qjlbfrXzMfQixx07z6zmj.epK/dSR/CB283JV.eNHA7BmRNmY7U9i', now(), 1); ");
		sql.append("INSERT INTO seguranca.usuario(id, email, login, nome, senha, ultimaalteracao, supermercado_id) VALUES (2, 'EDUARDO.YUCHO@outlook.com', 'sys2', 'EDUARDO YUCHO', '$2a$10$qjlbfrXzMfQixx07z6zmj.epK/dSR/CB283JV.eNHA7BmRNmY7U9i', now(), 2); ");
		sql.append("INSERT INTO seguranca.usuario(id, email, login, nome, senha, ultimaalteracao, supermercado_id) VALUES (3, 'EDUARDO.YUCHO@outlook.com', 'sys3', 'EDUARDO YUCHO', '$2a$10$qjlbfrXzMfQixx07z6zmj.epK/dSR/CB283JV.eNHA7BmRNmY7U9i', now(), 3); ");
		sql.append("INSERT INTO seguranca.usuario(id, email, login, nome, senha, ultimaalteracao, supermercado_id) VALUES (4, 'EDUARDO.YUCHO@outlook.com', 'sys4', 'EDUARDO YUCHO', '$2a$10$qjlbfrXzMfQixx07z6zmj.epK/dSR/CB283JV.eNHA7BmRNmY7U9i', now(), 4); ");
		sql.append("INSERT INTO seguranca.usuario(id, email, login, nome, senha, ultimaalteracao, supermercado_id) VALUES (5, 'EDUARDO.YUCHO@outlook.com', 'sys5', 'EDUARDO YUCHO', '$2a$10$qjlbfrXzMfQixx07z6zmj.epK/dSR/CB283JV.eNHA7BmRNmY7U9i', now(), 5); ");
		sql.append("INSERT INTO seguranca.usuario(id, email, login, nome, senha, ultimaalteracao, supermercado_id) VALUES (6, 'EDUARDO.YUCHO@outlook.com', 'admin', 'EDUARDO YUCHO', '$2a$10$qjlbfrXzMfQixx07z6zmj.epK/dSR/CB283JV.eNHA7BmRNmY7U9i', now(), 5); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e1) {
			return;
		}
		sql = new StringBuilder();
		sql.append("INSERT INTO seguranca.usuario_autoridade(id, authority, usuario_id) VALUES (1, 'ROLE_USER', 1); ");
		sql.append("INSERT INTO seguranca.usuario_autoridade(id, authority, usuario_id) VALUES (2, 'ROLE_USER', 2); ");
		sql.append("INSERT INTO seguranca.usuario_autoridade(id, authority, usuario_id) VALUES (3, 'ROLE_USER', 3); ");
		sql.append("INSERT INTO seguranca.usuario_autoridade(id, authority, usuario_id) VALUES (4, 'ROLE_USER', 4); ");
		sql.append("INSERT INTO seguranca.usuario_autoridade(id, authority, usuario_id) VALUES (5, 'ROLE_USER', 5); ");
		sql.append("INSERT INTO seguranca.usuario_autoridade(id, authority, usuario_id) VALUES (6, 'ROLE_USER', 6); ");
		try {
			conn.prepareStatement(sql.toString()).executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void initSequence(Connection conn) throws SQLException {
		List<String> lista = new ArrayList<>();
		lista.add("SELECT setval('cadastro.bairro_id_seq', 1, false); ");
		lista.add("SELECT setval('cadastro.cep_id_seq', 1, false); ");
		lista.add("SELECT setval('cadastro.municipio_id_seq', 1, false); ");
		lista.add("SELECT setval('cadastro.cliente_id_seq', 1, false); ");
		lista.add("SELECT setval('cadastro.cliente_endereco_id_seq', 1, false); ");
		lista.add("SELECT setval('cadastro.produto_id_seq', 1, false); ");
		lista.add("SELECT setval('cadastro.forma_pagamento_id_seq', 1, false); ");
		lista.add("SELECT setval('cadastro.supermercado_id_seq', 1, false); ");
		lista.add("SELECT setval('cadastro.supermercado_endereco_id_seq', 1, false); ");
		lista.add("SELECT setval('faturamento.oferta_id_seq', 1, false); ");
		lista.add("SELECT setval('faturamento.oferta_produto_id_seq', 1, false); ");
		lista.add("SELECT setval('faturamento.pedido_id_seq', 1, false); ");
		lista.add("SELECT setval('faturamento.pedido_item_id_seq', 1, false); ");
		for (String string : lista) {
			PreparedStatement st = conn.prepareStatement(string);
			st.executeQuery();
		}
	}

}
