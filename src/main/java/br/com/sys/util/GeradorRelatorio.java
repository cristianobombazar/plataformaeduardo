package br.com.sys.util;

import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class GeradorRelatorio {

	private List<?> lista;

	public GeradorRelatorio(List<?> lista) {
		this.lista = lista;
	}

	public JasperPrint gerarRelatorio(String nomeRelatorio, Map<String, Object> parametros) throws JRException {
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(this.lista);
		return JasperFillManager.fillReport(getClass().getResourceAsStream("/relatorios/" + nomeRelatorio + ".jasper"), parametros, ds);
	}

}
