package br.com.sys.util;

import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import com.ibm.icu.text.SimpleDateFormat;

public class DateUtil {

	public static final String defaultPattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX";
	public static final String datePattern = "yyyy-MM-dd";

	public static final String TIMEZONE_NAME = TimeZone.getDefault().getDisplayName();
	public static SimpleDateFormat sdf = null;

	public static LocalDate toLocalDate(Date date) {
		return LocalDate.from(Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()));
	}

	public static LocalDateTime toLocalDateTime(Date date) {
		return LocalDateTime.from(Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()));
	}

	public static Date toDate(LocalDate lDate) {
		return Date.from(lDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public static Date toDate(LocalDateTime lDate) {
		return Date.from(lDate.atZone(ZoneId.systemDefault()).toInstant());
	}

	public static Date toDateBD(String data) throws ParseException {
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.parse(data);
	}

}
