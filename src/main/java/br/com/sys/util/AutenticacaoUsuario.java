package br.com.sys.util;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import br.com.sys.model.seguranca.Usuario;

@SuppressWarnings("serial")
public class AutenticacaoUsuario implements Authentication {
	
	private final Usuario usuario;
	private boolean autenticado = true;
	
	public AutenticacaoUsuario(Usuario usuario){
		this.usuario = usuario;
	}

	@Override
	public String getName() {
		return usuario.getLogin();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return usuario.getAuthorities();
	}

	@Override
	public String getCredentials() {
		return usuario.getSenha();
	}

	@Override
	public String getDetails() {
		return usuario.getEmail();
	}

	@Override
	public Usuario getPrincipal() {
		return usuario;
	}

	@Override
	public boolean isAuthenticated() {
		return autenticado;
	}

	@Override
	public void setAuthenticated(boolean autenticado) throws IllegalArgumentException {
		this.autenticado = autenticado;
	}

}
