package br.com.sys.util;

import java.time.LocalTime;
import java.sql.Time;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LocalTimeAttributeConverter implements AttributeConverter<LocalTime, Time> {

	@Override
	public Time convertToDatabaseColumn(LocalTime locDate) {
		return (locDate == null ? null : Time.valueOf(locDate));
	}

	@Override
	public LocalTime convertToEntityAttribute(Time sqlDate) {
		return (sqlDate == null ? null : sqlDate.toLocalTime());
	}
}