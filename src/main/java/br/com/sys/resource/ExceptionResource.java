package br.com.sys.resource;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.sys.exception.AlertWrapper;

@ControllerAdvice
public class ExceptionResource {

	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public AlertWrapper handleException(Exception ex) {
		ex.printStackTrace(System.err);
		if(ex instanceof DataIntegrityViolationException){
			DataIntegrityViolationException divEx = (DataIntegrityViolationException) ex;
			return new AlertWrapper(AlertWrapper.TIPO_WARNING, divEx.getMostSpecificCause());
		}
		return new AlertWrapper(AlertWrapper.TIPO_WARNING, ex);
	}
}
