package br.com.sys.resource;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.sys.model.seguranca.Usuario;
import br.com.sys.resource.interfaces.CrudController;
import br.com.sys.service.interfaces.CrudService;

import org.springframework.data.domain.Sort.Order;

public abstract class DefaultCrudResource<T, ID extends Serializable> implements CrudController<T, ID> {
	private CrudService<T, ID> service;

	public DefaultCrudResource(CrudService<T, ID> service) {
		this.service = service;
	}

	@RequestMapping(value = "/", method = RequestMethod.GET, params = { "pagina", "tamanho" })
	public Page<T> consultarTodos(Integer pagina, Integer tamanho, @RequestParam(required = false) String sort, @RequestParam(required = false) String direction) throws Exception {
		if (StringUtils.isNotEmpty(sort)) {
			List<Order> orders = new ArrayList<>();
			orders.add(new Order(Sort.Direction.fromStringOrNull(direction), "id"));
			return service.findAll(pagina, tamanho, orders);
		} else {
			return service.findAll(pagina, tamanho);
		}
	}

	@Override
	@RequestMapping(method = RequestMethod.GET)
	public List<T> findAll() throws Exception {
		return service.findAll();
	}

	@Override
	@RequestMapping(value = "/{id:\\d+}", method = RequestMethod.GET)
	public T find(@PathVariable ID id) throws Exception {
		return service.findOne(id);
	}

	@Override
	@RequestMapping(value = "/{id:\\d+}", method = RequestMethod.DELETE)
	public void delete(@PathVariable ID id) throws Exception {
		service.delete(id);
	}

	@Override
	@RequestMapping(method = RequestMethod.POST)
	public T save(@RequestBody T object) throws Exception {
		return service.save(object);
	}

	protected Usuario getUsuarioLogado() {
		return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
