package br.com.sys.resource.seguranca;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.sys.model.seguranca.Usuario;
import br.com.sys.resource.DefaultCrudResource;
import br.com.sys.service.interfaces.seguranca.UsuarioService;

@CrossOrigin
@RestController
@RequestMapping(value = "/cadastro/usuario")
public class UsuarioResource extends DefaultCrudResource<Usuario, Integer> {

	private UsuarioService service;
	
	@Autowired
	public UsuarioResource(UsuarioService service) {
		super(service);
		this.service = service;
	}
	
	@RequestMapping(value ="/findUserByLogin", method = RequestMethod.GET)
	public Usuario getUsuario(@RequestParam("login") String loginOrEmail) {
		return service.getUsuario(loginOrEmail);
	}

}
