package br.com.sys.resource;

import java.util.Date;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sys.configuration.ConnectionManager;

@CrossOrigin
@RestController
public class RootController {

	@RequestMapping("/")
	public ServerInfo index() {
		return new ServerInfo();
	}

	public class ServerInfo {
		private final ConnectionManager connManager = ConnectionManager.getInstance();
		
		public String getSistema() {
			return "PlataformaEduardo";
		}

		public String getVersao() {
			return "1.0.0-BETA";
		}
		
		public int getVersaoDb(){
			return connManager.getVersaoDb();
		}
		
		public Date getDataHora(){
			return new Date();
		}
		
		public String getSenha() {
			return BCrypt.hashpw("sys", BCrypt.gensalt());
		}

	}
}
