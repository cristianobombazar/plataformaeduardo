package br.com.sys.resource.relatorio.faturamento;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import br.com.sys.dto.filtro.Filtro;
import br.com.sys.model.seguranca.Usuario;
import br.com.sys.service.interfaces.cadastro.ProdutoService;
import br.com.sys.service.interfaces.faturamento.PedidoService;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

@CrossOrigin
@RestController
@RequestMapping(value = "/relatorio/faturamento")
public class RelatorioFaturamento {
	
	@Autowired
	private ProdutoService produtoService;
	
	@Autowired
	private PedidoService pedidoService;

	@RequestMapping(value="/relatorioProdutoVendido", method = RequestMethod.POST)
	public void relatorioProdutoVendido(@RequestBody Filtro filtro, HttpServletResponse response, HttpServletRequest request) throws Exception {
		JasperPrint relatorio = produtoService.relatorioProdutoVendido(filtro.getDataInicial(), filtro.getDataFinal(), getUsuarioLogado().getSupermercado());
		response.setContentType("application/pdf");
		if (relatorio == null) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}else {
			JasperExportManager.exportReportToPdfStream(relatorio, response.getOutputStream());
		}
	}
	
	@RequestMapping(value="/relatorioPedido", method = RequestMethod.POST)
	public void relatorioPedido(@RequestBody Filtro filtro, HttpServletResponse response, HttpServletRequest request) throws Exception {
		filtro.setSupermercado(getUsuarioLogado().getSupermercado());
		JasperPrint relatorio = pedidoService.relatorioPedido(filtro);
		response.setContentType("application/pdf");
		if (relatorio == null) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}else {
			JasperExportManager.exportReportToPdfStream(relatorio, response.getOutputStream());
		}
	}
	
	protected Usuario getUsuarioLogado() {
		return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	 
}
