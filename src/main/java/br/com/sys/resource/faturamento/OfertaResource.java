package br.com.sys.resource.faturamento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.model.faturamento.Oferta;
import br.com.sys.resource.DefaultCrudResource;
import br.com.sys.service.interfaces.faturamento.OfertaService;

@CrossOrigin
@RestController
@RequestMapping(value = "/faturamento/oferta")
public class OfertaResource extends DefaultCrudResource<Oferta, Integer> {

	private OfertaService service;
	
	@Autowired
	public OfertaResource(OfertaService service) {
		super(service);
		this.service = service;
	}
	
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public List<Oferta> findAll() throws Exception {
		return service.findAll(getUsuarioLogado().getSupermercado());
	}
	
	
	@RequestMapping(value="/findOferta", method = RequestMethod.GET)
	public Oferta findOferta(@RequestParam(name = "supermercadoID") Integer supermercadoID) throws Exception {
		Supermercado supermercado = new Supermercado();
		supermercado.setId(supermercadoID);
		return service.findOferta(supermercado);
	}
	
	@RequestMapping(value="/findOfertaByQrCode", method = RequestMethod.GET)
	public Oferta findOferta(@RequestParam(name = "qrCode") Long qrCode) throws Exception {
		return service.findOferta(qrCode);
	}

	@Override
	@RequestMapping(method = RequestMethod.POST)
	public Oferta save(@RequestBody Oferta oferta) throws Exception {
		if (oferta != null && oferta.getProdutos() != null && !oferta.getProdutos().isEmpty()) {
			oferta.getProdutos().forEach(item ->{
				item.setOferta(oferta);
			});
		}
		oferta.setSupermercado(getUsuarioLogado().getSupermercado());
		return super.save(oferta);
	}
	
	@RequestMapping(value = "/item/{id:\\d+}", method = RequestMethod.DELETE)
	public void deletarItem(@PathVariable Integer id) throws Exception {
		service.deletarItem(id);
	}
	
}
