package br.com.sys.resource.faturamento;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sys.model.cadastro.Cliente;
import br.com.sys.model.faturamento.Pedido;
import br.com.sys.resource.DefaultCrudResource;
import br.com.sys.service.interfaces.cadastro.ClienteService;
import br.com.sys.service.interfaces.faturamento.PedidoService;

@CrossOrigin
@RestController
@RequestMapping(value = "/faturamento/pedido")
public class PedidoResource extends DefaultCrudResource<Pedido, Integer> {

	private PedidoService service;
	
	@Autowired
	private ClienteService clienteService;
	
	@Autowired
	public PedidoResource(PedidoService service) {
		super(service);
		this.service = service;
	}
	
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public List<Pedido> findAll() throws Exception {
		return service.findAll(getUsuarioLogado().getSupermercado());
	}
	

	@Override
	@RequestMapping(method = RequestMethod.POST)
	public Pedido save(@RequestBody Pedido pedido) throws Exception {
		if (pedido != null && pedido.getItens() != null && !pedido.getItens().isEmpty()) {
			pedido.getItens().forEach(item ->{
				item.setPedido(pedido);
			});
		}
		pedido.setSupermercado(getUsuarioLogado().getSupermercado());
		return super.save(pedido);
	}
	
	@RequestMapping(value = "/forMobile", method = RequestMethod.POST)
	public Pedido saveForMobile(@RequestBody Pedido pedido) throws Exception {
		if (pedido != null && pedido.getItens() != null && !pedido.getItens().isEmpty()) {
			pedido.getItens().forEach(item ->{
				item.setPedido(pedido);
			});
		}
		Cliente cliente = clienteService.save(pedido.getCliente());
		pedido.setCliente(cliente);
		return super.save(pedido);
	}
	
	@RequestMapping(value = "/item/{id:\\d+}", method = RequestMethod.DELETE)
	public void deletarItem(@PathVariable Integer id) throws Exception {
		service.deletarItem(id);
	}
	
}
