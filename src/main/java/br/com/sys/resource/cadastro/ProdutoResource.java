package br.com.sys.resource.cadastro;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sys.model.cadastro.Produto;
import br.com.sys.resource.DefaultCrudResource;
import br.com.sys.service.interfaces.cadastro.ProdutoService;

@CrossOrigin
@RestController
@RequestMapping(value = "/cadastro/produto")
public class ProdutoResource extends DefaultCrudResource<Produto, Integer> {

	private ProdutoService service;
	
	@Autowired
	public ProdutoResource(ProdutoService service) {
		super(service);
		this.service = service;
	}
	
	@RequestMapping(value = "/generateQrCodeProduto", params = {"generatedValue", "descricao"}, method = RequestMethod.GET, produces = "application/json")
	public Produto generateQrCodeProduto(Long generatedValue, String descricao) throws Exception{
		String base = service.generateBase64QrCode(generatedValue, descricao, getUsuarioLogado().getSupermercado().getRazaoSocial());
		Produto produto = new Produto();
		produto.setQrCode(base);
		return produto;
	}
	
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public List<Produto> findAll() throws Exception {
		return service.findAll(getUsuarioLogado().getSupermercado());
	}
	
	@Override
	@RequestMapping(method = RequestMethod.POST)
	public Produto save(@RequestBody Produto produto) throws Exception {
		if (produto.getQrCode() == null || produto.getQrCode().trim().isEmpty()) {
			produto.setGeneratedValueQrCodeEncoder(new Date().getTime());
			produto.setQrCode(service.generateBase64QrCode(produto.getGeneratedValueQrCodeEncoder(), produto.getDescricao(), getUsuarioLogado().getSupermercado().getRazaoSocial()));
		}
		produto.setSupermercado(getUsuarioLogado().getSupermercado());
		return super.save(produto);
	}

}
