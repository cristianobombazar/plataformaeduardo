package br.com.sys.resource.cadastro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sys.model.cadastro.ClienteCartao;
import br.com.sys.resource.DefaultCrudResource;
import br.com.sys.service.interfaces.cadastro.ClienteCartaoService;

@CrossOrigin
@RestController
@RequestMapping(value = "/cadastro/clienteCartao")
public class ClienteCartaoResource extends DefaultCrudResource<ClienteCartao, Integer> {

	@Autowired
	public ClienteCartaoResource(ClienteCartaoService serivce) {
		super(serivce);
	}
	
	@Override
	@RequestMapping(value = "/{id:\\d+}", method = RequestMethod.DELETE)
	public void delete(@PathVariable Integer id) throws Exception {
		super.delete(id);
	}

}
