package br.com.sys.resource.cadastro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sys.model.cadastro.Bairro;
import br.com.sys.model.cadastro.Cep;
import br.com.sys.model.cadastro.Municipio;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.model.cadastro.SupermercadoEndereco;
import br.com.sys.resource.DefaultCrudResource;
import br.com.sys.service.interfaces.cadastro.BairroService;
import br.com.sys.service.interfaces.cadastro.CepService;
import br.com.sys.service.interfaces.cadastro.MunicipioService;
import br.com.sys.service.interfaces.cadastro.SupermercadoService;
import br.com.sys.service.interfaces.util.UtilService;

@CrossOrigin
@RestController
@RequestMapping(value = "/cadastro/supermercado")
public class SupermercadoResource extends DefaultCrudResource<Supermercado, Integer> {

	@Autowired
	private UtilService utilService;
	
	@Autowired
	private MunicipioService municipioService;
	
	@Autowired
	private BairroService bairroService;
	
	@Autowired
	private CepService cepService;
	
	@Autowired
	public SupermercadoResource(SupermercadoService serivce) {
		super(serivce);
	}
	
	@Override
	@RequestMapping( method = RequestMethod.POST)
	public Supermercado save(@RequestBody Supermercado supermercado) throws Exception {
		Municipio municipio = utilService.findMunicipioByNome(supermercado.getEndereco().getMunicipio().getNome());
		Bairro    bairro    = utilService.findBairroByNome(supermercado.getEndereco().getBairro().getNome());
		Cep       cep       = utilService.findCepByCodigo(supermercado.getEndereco().getCep().getCodigo());
		if (municipio == null) {
			supermercado.getEndereco().getMunicipio().setId(null);
			municipio = municipioService.save(supermercado.getEndereco().getMunicipio());
		}
		if (bairro == null) {
			supermercado.getEndereco().getBairro().setId(null);
			bairro = bairroService.save(supermercado.getEndereco().getBairro());
		}
		if (cep == null) {
			supermercado.getEndereco().getCep().setId(null);
			cep = cepService.save(supermercado.getEndereco().getCep());
		}
		SupermercadoEndereco superEndereco = supermercado.getEndereco();
		superEndereco.setSupermercado(supermercado);
		superEndereco.setBairro(bairro);		
		superEndereco.setMunicipio(municipio);
		superEndereco.setCep(cep);
		supermercado.setEndereco(superEndereco);
		
		return super.save(supermercado);
	}

}
