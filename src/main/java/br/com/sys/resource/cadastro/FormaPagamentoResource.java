package br.com.sys.resource.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.sys.model.cadastro.FormaPagamento;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.resource.DefaultCrudResource;
import br.com.sys.service.interfaces.cadastro.FormaPagamentoService;

@CrossOrigin
@RestController
@RequestMapping(value = "/cadastro/formaPagamento")
public class FormaPagamentoResource extends DefaultCrudResource<FormaPagamento, Integer> {

	private FormaPagamentoService service;
	
	@Autowired
	public FormaPagamentoResource(FormaPagamentoService service) {
		super(service);
		this.service = service;
	}
	
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public List<FormaPagamento> findAll() throws Exception {
		return service.findAll(getUsuarioLogado().getSupermercado());
	}
	
	@Override
	@RequestMapping(method = RequestMethod.POST)
	public FormaPagamento save(@RequestBody FormaPagamento formaPagamento) throws Exception {
		formaPagamento.setSupermercado(getUsuarioLogado().getSupermercado());
		return super.save(formaPagamento);
	}
	
	@RequestMapping(value = "/findAllBySupermercado", method = RequestMethod.GET)
	public List<FormaPagamento> findAllBySupermercado(@RequestParam Integer supermercadoID) throws Exception {
		Supermercado supermercado = new Supermercado();
		supermercado.setId(supermercadoID);
		return service.findAll(supermercado);
	}

}
