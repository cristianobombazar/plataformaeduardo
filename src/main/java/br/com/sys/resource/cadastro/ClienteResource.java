package br.com.sys.resource.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import br.com.sys.model.cadastro.Cliente;
import br.com.sys.resource.DefaultCrudResource;
import br.com.sys.service.interfaces.cadastro.ClienteCartaoService;
import br.com.sys.service.interfaces.cadastro.ClienteService;

@CrossOrigin
@RestController
@RequestMapping(value = "/cadastro/cliente")
public class ClienteResource extends DefaultCrudResource<Cliente, Integer> {

	@Autowired
	private ClienteCartaoService clienteCartaoService;
	
	private ClienteService service;
	
	@Autowired
	public ClienteResource(ClienteService service) {
		super(service);
		this.service = service;
	}
	
	@Override
	public List<Cliente> findAll() throws Exception {
		return service.findAll(getUsuarioLogado().getSupermercado());
	}
	
	@Override
	@RequestMapping( method = RequestMethod.POST)
	public Cliente save(@RequestBody Cliente cliente) throws Exception {
		return service.save(cliente);
	}
	
	@RequestMapping(value = "deletarCartao/{id:\\d+}", method = RequestMethod.DELETE)
	public void deletarCartaoCliente(@PathVariable Integer id) throws Exception {
		clienteCartaoService.deletarCartao(id);
	}

}
