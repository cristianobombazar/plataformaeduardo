package br.com.sys.resource.interfaces;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @CrossOrigin
 * @RestController
 * @RequestMapping(value = "/cadastro/XXXX")
 */
public interface CrudController<T, ID> {

	/**
	 * @RequestMapping(value = "/consultarTodos", method = RequestMethod.GET)
	 */
	public List<T> findAll() throws Exception;

	/**
	 * @RequestMapping(value = "/consultar/{id:\\d+}", method =
	 *                       RequestMethod.GET)
	 */
	public T find(@PathVariable ID id) throws Exception;

	/**
	 * @RequestMapping(value = "/excluir/{id:\\d+}", method =
	 *                       RequestMethod.DELETE)
	 */
	public void delete(@PathVariable ID id) throws Exception;

	/**
	 * @RequestMapping(value = "/salvar", method = RequestMethod.POST)
	 */
	public T save(@RequestBody T t) throws Exception;
}
