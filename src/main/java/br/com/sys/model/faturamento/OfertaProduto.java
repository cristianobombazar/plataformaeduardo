package br.com.sys.model.faturamento;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.sys.model.cadastro.Produto;

@Entity
@Table(schema = "faturamento", name = "oferta_produto")
public class OfertaProduto {

	@Id
	@SequenceGenerator(sequenceName = "oferta_produto_id_seq", name = "oferta_produto_id_seq", schema = "faturamento", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "oferta_produto_id_seq")
	private Integer id;
	
	@ManyToOne
	@Valid
	@NotNull
	@JsonBackReference
	private Oferta oferta;
	
	@ManyToOne
	@Valid
	@NotNull
	private Produto produto;
	
	@NotNull(message = "Valor do produto n�o pode ser nulo")
	@Column(name = "valor", insertable = true, updatable = true, nullable = false, precision = 11, scale = 2)	
	private BigDecimal valor;
	
	@NotNull(message = "Quantidade do produto n�o pode ser nulo")
	@Column(name = "quantidade", insertable = true, updatable = true, nullable = false, precision = 11, scale = 2)
	private BigDecimal quantidade;
	
	@NotNull(message = "Quantidade Dispon�vel do produto n�o pode ser nulo")
	@Column(name = "quantidadedisponivel", insertable = true, updatable = true, nullable = false, precision = 11, scale = 2)
	private BigDecimal quantidadeDisponivel;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Oferta getOferta() {
		return oferta;
	}
	public void setOferta(Oferta oferta) {
		this.oferta = oferta;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public BigDecimal getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	
	public void setQuantidadeDisponivel(BigDecimal quantidadeDisponivel) {
		this.quantidadeDisponivel = quantidadeDisponivel;
	}
	
	public BigDecimal getQuantidadeDisponivel() {
		return quantidadeDisponivel;
	}
	
}
