package br.com.sys.model.faturamento;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.sys.model.cadastro.Produto;

@Entity
@Table(schema = "faturamento", name = "pedido_item")
public class PedidoItem {

	@Id
	@SequenceGenerator(sequenceName = "pedido_item_id_seq", name = "pedido_item_id_seq", schema = "faturamento", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pedido_item_id_seq")
	private Integer id;
	
	@ManyToOne
	@Valid
	@NotNull
	private Produto produto;
	
	@ManyToOne
	@Valid
	@NotNull
	@JsonBackReference
	private Pedido pedido;
		
	
	@NotNull(message = "Valor do produto n�o pode ser nulo")
	@Column(name = "valor", insertable = true, updatable = true, nullable = false, precision = 11, scale = 2)
	private BigDecimal valor;
	
	@NotNull(message = "Quantidade do produto n�o pode ser nulo")
	@Column(name = "quantidade", insertable = true, updatable = true, nullable = false, precision = 11, scale = 2)
	private BigDecimal quantidade;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public Pedido getPedido() {
		return pedido;
	}
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public BigDecimal getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	
	public BigDecimal getValorTotal() {
		return quantidade.multiply(valor);
	}
	
}
