package br.com.sys.model.faturamento;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.sys.model.cadastro.Cliente;
import br.com.sys.model.cadastro.FormaPagamento;
import br.com.sys.model.cadastro.Supermercado;

@Entity
@Table(schema = "faturamento", name = "pedido")
public class Pedido {
	
	public static final int STATUS_PEDIDO_PENDENTE = 1;
	public static final int STATUS_PEDIDO_LIBERADO = 2;
	public static final int STATUS_PEDIDO_FATURADO = 3;
	public static final int STATUS_PEDIDO_CANCELADO = 4;
	
	@Id
	@SequenceGenerator(sequenceName = "pedido_id_seq", name = "pedido_id_seq", schema = "faturamento", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pedido_id_seq")
	private Integer id;

	@ManyToOne
	@Valid
	@NotNull
	private Cliente cliente;
	
	@ManyToOne
	@Valid
	@NotNull
	private Supermercado supermercado;
	
	@ManyToOne
	@Valid
	@NotNull
	private FormaPagamento formaPagamento;
	
	@ManyToOne
	@Valid
	@NotNull
	private Oferta oferta;
	
	@Column(name = "situacao", insertable = true, updatable = true, nullable = false)
	private Integer situacao;
	
	@NotNull(message = "Situacao do pedido n�o pode ser nulo")	
	private Date data;
	
	@Size(min = 0, max = 255, message = "Obs do produto deve possuir entre 0 e 255 caractres")
	@Column(name = "obs", insertable = true, updatable = true, nullable = true, length = 255)
	private String obs;
	
	@OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<PedidoItem> itens = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public Supermercado getSupermercado() {
		return supermercado;
	}

	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}

	public Integer getSituacao() {
		return situacao;
	}

	public void setSituacao(Integer situacao) {
		this.situacao = situacao;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public List<PedidoItem> getItens(){
		return itens;
	}
	
	public void adicionaItem(PedidoItem item) {
		itens.add(item);
	}
	
	public void removeItem(PedidoItem item) {
		itens.remove(item);
	}
	
	public Integer getQuantidadeItens() {
		return itens.size();
	}
	
	public BigDecimal getQuantidadeTotal() {
		return itens.stream().map(PedidoItem::getQuantidade).reduce(BigDecimal.ZERO, BigDecimal::add);
	}
	
	public BigDecimal getValorTotal() {
		return itens.stream().map(PedidoItem::getValorTotal).reduce(BigDecimal.ZERO, BigDecimal::add);
	}
	
	public void setOferta(Oferta oferta) {
		this.oferta = oferta;
	}
	
	public Oferta getOferta() {
		return oferta;
	}
}
