package br.com.sys.model.faturamento;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.util.DateUtil;

@Entity
@Table(schema = "faturamento", name = "oferta")
public class Oferta {
	
	@Id
	@SequenceGenerator(sequenceName = "oferta_id_seq", name = "oferta_id_seq", schema = "faturamento", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "oferta_id_seq")
	private Integer id;
	
	@Size(min = 1, max = 60, message = "Descri��o da oferta deve possuir entre 1 e 60 caractres")
	@NotNull(message = "Descri��o da Oferta n�o pode ser nula")
	@Column(name = "descricao", updatable = true, insertable = true, nullable = false, length = 60)
	private String descricao;
	
	@NotNull(message = "Data Cadastro da Oferta n�o pode ser nula")
	@JsonFormat(pattern = DateUtil.datePattern)
	private Date dataCadastro;
	
	@NotNull(message = "Data Inicial da Oferta n�o pode ser nula")
	@JsonFormat(pattern = DateUtil.datePattern)
	private Date dataInicio;
	
	@NotNull(message = "Data Fim da Oferta n�o pode ser nula")
	@JsonFormat(pattern = DateUtil.datePattern)
	private Date dataFim;
	
	@ManyToOne
	@Valid
	@NotNull
	private Supermercado supermercado;
	
	@Size(min = 0, max = 255, message = "Obs do produto deve possuir entre 0 e 255 caractres")
	@Column(name = "obs", insertable = true, updatable = true, nullable = true, length = 255)
	private String obs;
	
	@OneToMany(mappedBy = "oferta", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Valid
	@NotEmpty
	private List<OfertaProduto> produtos = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public Supermercado getSupermercado() {
		return supermercado;
	}
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	public String getObs() {
		return obs;
	}
	public void setObs(String obs) {
		this.obs = obs;
	}
	
	public List<OfertaProduto> getProdutos(){
		return produtos;
	}
	
	public void adicionaOfertaProduto(OfertaProduto ofertaProduto) {
		produtos.add(ofertaProduto);
	}
	
	public void removerOfertaProduto(OfertaProduto ofertaProduto) {
		produtos.remove(ofertaProduto);
	}
	

}
