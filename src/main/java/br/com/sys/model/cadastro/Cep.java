package br.com.sys.model.cadastro;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(schema = "cadastro", name = "cep")
public class Cep {

	@Id
	@SequenceGenerator(name = "cep_id_seq", sequenceName = "cep_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cep_id_seq")
	private Integer id;
	
	@NotEmpty(message = "C�digo do cep n�o pode ser nulo")
	@Column(name = "codigo", nullable = false, updatable = true, insertable = true)
	private Integer codigo;
	
	public Cep() {
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	
	
}
