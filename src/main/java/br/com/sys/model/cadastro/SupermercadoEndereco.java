package br.com.sys.model.cadastro;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(schema = "cadastro", name = "supermercado_endereco")
public class SupermercadoEndereco {
	
	@Id
	@SequenceGenerator(name = "supermercado_endereco_id_seq", sequenceName = "supermercado_endereco_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "supermercado_endereco_id_seq")
	private Integer id;
	
	@OneToOne
	@JsonBackReference
	@NotNull
	@Valid
	private Supermercado supermercado;
	
	@ManyToOne
	@NotNull
	@Valid
	private Municipio municipio;
	
	@ManyToOne
	@NotNull
	@Valid
	private Bairro bairro;
	
	@ManyToOne
	@NotNull
	@Valid
	private Cep cep;
	
	@NotNull(message = "Rua do supermercado n�o pode ser nula")
	@Size(min = 1, max = 60, message = "Rua do supermercado deve possuir entre 1 e 60 caracteres")
	@Column(name = "rua", insertable = true, updatable = true, nullable = false, length = 60)
	private String rua;
	
	@NotNull(message = "Complemento do supermercado n�o pode ser nulo")
	@Size(min = 1, max = 60, message = "Complemento do supermercado deve possuir entre 1 e 60 caracteres")
	@Column(name = "complemento", insertable = true, updatable = true, nullable = false, length = 60)
	private String complemento;
	
	@Column(name = "numero", insertable = true, updatable = true, nullable = false, length = 5)
	private String numero;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Supermercado getSupermercado() {
		return supermercado;
	}
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public Bairro getBairro() {
		return bairro;
	}
	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}
	public Cep getCep() {
		return cep;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	

}
