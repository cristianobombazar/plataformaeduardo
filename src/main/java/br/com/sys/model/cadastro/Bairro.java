package br.com.sys.model.cadastro;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(schema = "cadastro", name = "bairro")
public class Bairro {
	
	@Id
	@SequenceGenerator(name = "bairro_id_seq", sequenceName = "bairro_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bairro_id_seq")
	private Integer id;

	@NotNull(message = "Nome do bairro n�o pode ser nulo")
	@Size(min = 1, max = 30, message = "Nome do bairro deve estar entre 1 e 30 caracteres")
	@Column(name = "nome",  insertable = true, updatable = true, nullable = false, length = 30)
	private String nome;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
