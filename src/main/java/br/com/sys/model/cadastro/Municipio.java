package br.com.sys.model.cadastro;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(schema = "cadastro", name = "municipio")
public class Municipio {

	@Id
	@SequenceGenerator(name = "municipio_id_seq", sequenceName = "municipio_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "municipio_id_seq")
	private Integer id;
	
	@NotNull(message = "Nome do municipio n�o pode ser nulo")
	@Size(min = 1, max = 40, message = "Nome do municipio deve possuir entre 1 e 40 caracteres")
	@Column(name = "nome", insertable = true, updatable = true, nullable = false, length = 40)
	private String nome;
	
	
	@NotNull(message = "UF do municipio n�o pode ser nulo")
	@Size(min = 2, max = 2, message = "Nome do municipio deve possuir 2 caractres")
	@Column(name = "uf", insertable = true, updatable = true, nullable = false, length = 2)
	private String uf;
	
	public Municipio() {
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setUf(String uf) {
		this.uf = uf;
	}
	
	public String getUf() {
		return uf;
	}
	
}
