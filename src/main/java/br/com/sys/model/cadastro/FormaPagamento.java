package br.com.sys.model.cadastro;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(schema = "cadastro", name = "forma_pagamento")
public class FormaPagamento {
	
	public static final int INDICADOR_USO_ATIVO = 1;
	public static final int INDICADOR_USO_INATIVO = 2;
	
	@Id
	@SequenceGenerator(name = "forma_pagamento_id_seq", sequenceName = "forma_pagamento_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "forma_pagamento_id_seq")
	private Integer id;
	
	@NotNull(message = "Descri��o da forma de pagamento n�o pode ser nulo")
	@Size(min = 1, max = 40, message = "Descri��o da forma de pagamento deve possuir entre 1 e 40 caracteres")
	@Column(name = "descricao", insertable = true, updatable = true, nullable = false, length = 40)
	private String descricao;
	
	@Column(name = "indicadoruso", insertable = true, updatable = true, nullable = false)
	private Integer indicadorUso;
	
	@NotNull
	@Column(name = "indicadorcartaocredito", insertable = true, updatable = true, nullable = false)
	private Integer indicadorCartaoCredito;
	
	@Valid
	@NotNull
	@ManyToOne
	private Supermercado supermercado;
	
	public FormaPagamento() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getIndicadorUso() {
		return indicadorUso;
	}

	public void setIndicadorUso(Integer indicadorUso) {
		this.indicadorUso = indicadorUso;
	}
	
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	
	public Supermercado getSupermercado() {
		return supermercado;
	}
	
	public void setIndicadorCartaoCredito(Integer indicadorCartaoCredito) {
		this.indicadorCartaoCredito = indicadorCartaoCredito;
	}
	
	public Integer getIndicadorCartaoCredito() {
		return indicadorCartaoCredito;
	}
	

}
