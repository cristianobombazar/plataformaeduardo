package br.com.sys.model.cadastro;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(schema = "cadastro", name = "cartao_cliente")
public class ClienteCartao {
	
	public static final Integer CARTAO_TIPO_DEBITO = 1;
	public static final Integer CARTAO_TIPO_CREDITO = 2;
	public static final Integer CARTAO_TIPO_ALIMENTACAO = 3;
	public static final Integer CARTAO_TIPO_OUTRO = 4;
	
	public static final Integer CARTAO_BANDEIRA_VISA = 1;
	public static final Integer CARTAO_BANDEIRA_MASTERCARD = 2;
	public static final Integer CARTAO_BANDEIRA_ELO = 3;
	public static final Integer CARTAO_BANDEIRA_AMERICAN_EXPRESS = 4;
	public static final Integer CARTAO_BANDEIRA_DISCOVERY_NETWORK = 5;
	public static final Integer CARTAO_BANDEIRA_DISCOVERY_OUTRO = 6;

	@Id
	@SequenceGenerator(name = "cliente_cartao_id_seq", sequenceName = "cliente_cartao_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cliente_cartao_id_seq")
	private Integer id;
	
	@NotNull(message = "Tipo do cart�o n�o pode ser nula")
	@Column(name = "tipo", insertable = true, updatable = true, nullable = false)
	private Integer tipo;
	
	@NotNull(message = "Bandeira do cart�o n�o pode ser nula")
	@Column(name = "bandeira", insertable = true, updatable = true, nullable = false)
	private Integer bandeira;
	
	@ManyToOne
	@JsonBackReference
	@Valid
	@NotNull
	private Cliente cliente;
	
	@NotNull(message = "N�mero do cart�o n�o pode ser nulo")
	@Size(min = 16, max = 16, message = "N�mero do cart�o deve possuir apenas 1 caracter")
	@Column(name = "numerocartao", insertable = true, updatable = true, nullable = false, length = 16)
	private String numeroCartao;
	
	@FutureOrPresent(message = "Data de expira��o do cart�o n�o pode ser retroativa")
	@NotNull(message = "Data da expira��o do cart�o n�o pode ser nula")
	@Column(name = "dataexpiracao", insertable = true, updatable = true, nullable = false)
	private Date dataExpiracao;
	
	@Column(name = "codigoseguranca", insertable = true, updatable = true, nullable = false, length = 3)
	private Integer codigoSeguranca;
	
	@NotNull(message = "Nome impresso do cart�o n�o pode ser nulo")
	@Size(min = 1, max = 50, message = "Nome deve impresso do cart�o deve possuir entre 1 e 50 caracteres")
	@Column(name = "nomeimpresso", insertable = true, updatable = true, nullable = false, length = 50)
	private String nomeImpresso;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getNumeroCartao() {
		return numeroCartao;
	}

	public void setNumeroCartao(String numeroCartao) {
		this.numeroCartao = numeroCartao;
	}

	public Date getDataExpiracao() {
		return dataExpiracao;
	}

	public void setDataExpiracao(Date dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}

	public Integer getCodigoSeguranca() {
		return codigoSeguranca;
	}

	public void setCodigoSeguranca(Integer codigoSeguranca) {
		this.codigoSeguranca = codigoSeguranca;
	}

	public String getNomeImpresso() {
		return nomeImpresso;
	}

	public void setNomeImpresso(String nomeImpresso) {
		this.nomeImpresso = nomeImpresso;
	}
	
	public void setBandeira(Integer bandeira) {
		this.bandeira = bandeira;
	}
	
	public Integer getBandeira() {
		return bandeira;
	}
	
}
