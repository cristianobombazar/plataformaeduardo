package br.com.sys.model.cadastro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(schema = "cadastro")
public class Cliente {
	
	public static final Integer CLIENTE_TIPO_FISICA = 1;
	public static final Integer CLIENTE_TIPO_JURIDICA = 2;
	
	public static final Integer INDICADOR_USO_ATIVO = 1;
	public static final Integer INDICADOR_USO_INATIVO = 2;
	
	@Id
	@SequenceGenerator(name = "cliente_id_seq", sequenceName = "cliente_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cliente_id_seq")
	private Integer id;
	
	@ManyToOne
	@Valid
	@NotNull(message = "Supermercado n�o pode ser nulo")
	private Supermercado supermercado;
	
	@NotNull(message = "Nome do cliente n�o pode ser nulo")
	@Size(min = 1, max = 50, message = "Nome deve possuir entre 1 e 50 caracteres")
	@Column(name = "nome", insertable = true, updatable = true, nullable = false, length = 50)
	private String nome;
	
	@NotNull(message = "Tipo do cliente n�o pode ser nulo")
	@Column(name = "tipo", insertable = true, updatable = true, nullable = false)	
	private Integer tipo;
	
	@NotNull(message = "CPF/CNPJ do cliente n�o pode ser nulo")
	@Size(min = 11, max = 18, message = "CPF/CNPJ deve possuir 18 caracteres")
	@Column(name = "cpfcnpj", insertable = true, updatable = true, nullable = false, length = 18)
	private String cpfCnpj;
	
	@Size(min = 9, max = 9, message = "RG deve possuir 9 caracteres")
	@Column(name = "rg", insertable = true, updatable = true, nullable = false, length = 9)
	private String rg;
	
	@NotNull(message = "Telefone do cliente n�o pode ser nulo")
	private String telefone;
	
	@Email
	@NotNull(message = "Email do cliente n�o pode ser nulo")
	@Size(min = 10, max = 80, message = "Voc� deve inserir um e-mail v�lido e que esteja entre 10 e 80 caracteres")
	private String email;
	
	@NotNull(message = "Status do cliente n�o pode ser nulo")
	@Column(name = "indicadoruso", insertable = true, updatable = true, nullable = false)
	private Integer indicadorUso;
	
	@OneToOne(mappedBy = "cliente", cascade = CascadeType.ALL)
	@NotNull(message = "Endere�o do cliente n�o pode ser nulo")
	@Valid
	private ClienteEndereco endereco;
	
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Valid
	private List<ClienteCartao> cartoes = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getRg() {
		return rg;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ClienteEndereco getEndereco() {
		return endereco;
	}
	public void setEndereco(ClienteEndereco endereco) {
		this.endereco = endereco;
	}
	
	public List<ClienteCartao> getCartoes() {
		return Collections.unmodifiableList(cartoes);
	}
	
	public void adicionarCartao(ClienteCartao cartao) {
		this.cartoes.add(cartao);
	}
	
	public void removerCartao(ClienteCartao cartao) {
		this.cartoes.remove(cartao);
	}
	
	public void setIndicadorUso(Integer indicadorUso) {
		this.indicadorUso = indicadorUso;
	}
	
	public Integer getIndicadorUso() {
		return indicadorUso;
	}
	
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	
	public Supermercado getSupermercado() {
		return supermercado;
	}
	
}
