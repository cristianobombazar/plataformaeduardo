package br.com.sys.model.cadastro;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(schema = "cadastro", name = "supermercado")
public class Supermercado {
	
	public static final Integer INDICADOR_USO_ATIVO = 1;
	public static final Integer INDICADOR_USO_INATIVO = 2;
	
	@Id
	@SequenceGenerator(name = "supermercado_id_seq", sequenceName = "supermercado_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "supermercado_id_seq")
	private Integer id;
	
	@NotNull(message = "Raz�o social do mercado n�o pode ser nulo")
	@Size(min = 1, max = 60, message = "Raz�o social do mercado deve possuir entre 1 e 60 caractres")
	@Column(name = "razaosocial", insertable = true, updatable = true, nullable = false, length = 60)
	private String razaoSocial;
	
	@NotNull(message = "CNPJ do cliente n�o pode ser nulo")
	@Size(min = 11, max = 14, message = "CNPJ deve possuir 14 caracteres")
	@Column(name = "cnpj", insertable = true, updatable = true, nullable = false, length = 14)
	private String cnpj;
	
	@NotEmpty(message = "Status do supermercado n�o pode ser nulo")
	@Column(name = "indicadoruso", insertable = true, updatable = true, nullable = false)
	private Integer indicadorUso;
	
	@OneToOne(mappedBy = "supermercado", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@NotNull
	@Valid
	private SupermercadoEndereco endereco;
	
	@Column(length = 10485760)
	private String logo;
	
	@PrePersist
	@PreUpdate
	public void preSave() {
		this.getEndereco().setSupermercado(this);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Integer getIndicadorUso() {
		return indicadorUso;
	}

	public void setIndicadorUso(Integer indicadorUso) {
		this.indicadorUso = indicadorUso;
	}

	public SupermercadoEndereco getEndereco() {
		return endereco;
	}

	public void setEndereco(SupermercadoEndereco endereco) {
		this.endereco = endereco;
	}
	
	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	public String getLogo() {
		return logo;
	}
	
}
