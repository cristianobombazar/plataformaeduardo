package br.com.sys.model.cadastro;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(schema = "cadastro", name = "cliente_endereco")
public class ClienteEndereco {
	
	@Id
	@SequenceGenerator(name = "cliente_endereco_id_seq", sequenceName = "cliente_endereco_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cliente_endereco_id_seq")
	private Integer id;
	
	@OneToOne
	@JsonBackReference
	@Valid
	@NotNull
	private Cliente cliente;
	
	@ManyToOne
	@Valid
	@NotNull
	
	private Municipio municipio;
	@ManyToOne
	@Valid
	@NotNull
	
	private Bairro bairro;
	@ManyToOne
	@Valid
	@NotNull
	private Cep cep;
	
	@NotNull(message = "Rua do cliente n�o pode ser nula")
	@Size(min = 1, max = 60, message = "Rua do cliente deve possuir entre 1 e 60 caracteres")
	@Column(name = "rua", insertable = true, updatable = true, nullable = false, length = 60)
	private String rua;
	
	@Size(min = 1, max = 60, message = "Complemento do cliente deve possuir entre 1 e 60 caracteres")
	@Column(name = "complemento", insertable = true, updatable = true, length = 60)
	private String complemento;
	
	@Column(name = "numero", insertable = true, updatable = true, nullable = false, length = 5)
	private String numero;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public Bairro getBairro() {
		return bairro;
	}
	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}
	public Cep getCep() {
		return cep;
	}
	public void setCep(Cep cep) {
		this.cep = cep;
	}
	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}

}
