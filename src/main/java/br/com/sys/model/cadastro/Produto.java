package br.com.sys.model.cadastro;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(schema = "cadastro", name = "produto")
public class Produto {
	
	
	@Id
	@SequenceGenerator(sequenceName = "produto_id_seq", name = "produto_id_seq", schema = "cadastro", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "produto_id_seq")
	private Integer id;
	
	@NotNull(message = "Descri��o do produto n�o pode ser nulo")
	@Size(min = 1, max = 60, message = "Descri��o do produto deve possuir entre 1 e 60 caracteres")
	@Column(name = "descricao", insertable = true, updatable = true, nullable = false, length = 60)
	private String descricao;
	
	@NotNull(message = "Valor do produto n�o pode ser nulo")
	@Column(name = "valor", insertable = true, updatable = true, nullable = false, precision = 11, scale = 2)	
	private BigDecimal valor;
	
	@NotNull(message = "Quantidade do produto n�o pode ser nulo")
	@Column(name = "quantidade", insertable = true, updatable = true, nullable = false, precision = 11, scale = 2)
	private BigDecimal quantidade;
	
	@NotNull(message = "Marca do produto n�o pode ser nula")
	@Size(min = 1, max = 30, message = "Marca do produto deve possuir entre 1 e 30 caracteres")
	@Column(name = "marca", insertable = true, updatable = true, nullable = false, length = 30)
	private String marca;
	
	@Column(length = 10485760)
	private String imagem;
	
	@NotNull
	@Column(length = 10485760)
	private String qrCode;
	
	@ManyToOne
	@Valid
	@NotNull
	private Supermercado supermercado;
	
	@Size(min = 0, max = 255, message = "Obs do produto deve possuir entre 0 e 255 caracteres")
	@Column(name = "obs", insertable = true, updatable = true, nullable = true, length = 255)
	private String obs;
	
	@NotNull
	private Long generatedValueQrCodeEncoder;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		return descricao;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public BigDecimal getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getImagem() {
		return imagem;
	}
	public String getQrCode() {
		return qrCode;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public Supermercado getSupermercado() {
		return supermercado;
	}
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	
	public void setObs(String obs) {
		this.obs = obs;
	}
	public String getObs() {
		return obs;
	}
	
	public void setGeneratedValueQrCodeEncoder(Long generatedValueQrCodeEncoder) {
		this.generatedValueQrCodeEncoder = generatedValueQrCodeEncoder;
	}
	
	public Long getGeneratedValueQrCodeEncoder() {
		return generatedValueQrCodeEncoder;
	}
}
