package br.com.sys.model.seguranca;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.sys.model.cadastro.Supermercado;

@SuppressWarnings("serial")
@Entity
@Table(name = "usuario", schema = "seguranca")
public class Usuario implements UserDetails {
	@Id
	@SequenceGenerator(sequenceName = "usuario_id_seq", name = "usuario_id_seq", schema = "seguranca", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usuario_id_seq")
	private Integer id;
	
	@NotNull(message = "Nome do usu�rio n�o pode ser nulo")
	@Size(min = 1, max = 50, message = "Nome do usu�rio deve possuir entre 1 e 50 caracteres")
	@Column(name = "nome", insertable = true, updatable = true, nullable = false, length = 50)
	private String nome;
	
	@Email
	@NotNull(message = "Email do usu�rio n�o pode ser nulo")
	@Size(min = 10, max = 80, message = "Voc� deve inserir um e-mail v�lido e que esteja entre 10 e 80 caracteres")
	private String email;
	
	@NotNull
	private String login;
	@NotNull	
	private String senha;
	@Temporal(TemporalType.TIMESTAMP)
	@FutureOrPresent
	private Date ultimaAlteracao;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@Valid
	@NotNull
	private Supermercado supermercado;

	@JsonManagedReference
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario", fetch = FetchType.EAGER, orphanRemoval = true)
	@Valid
	@NotEmpty
	private Set<UsuarioAutoridade> autoridades;

	@Override
	@JsonIgnore
	public String getUsername() {
		return login;
	}

	@Override
	@JsonIgnore
	public String getPassword() {
		return senha;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	@JsonIgnore
	public boolean isEnabled() {
		return true;
	}

	@Override
	@JsonIgnore
	public Collection<UsuarioAutoridade> getAuthorities() {
		return autoridades;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getUltimaAlteracao() {
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(Date ultimaAlteracao) {
		this.ultimaAlteracao = ultimaAlteracao;
	}

	public Set<UsuarioAutoridade> getAutoridades() {
		return autoridades;
	}

	public void setAutoridades(Set<UsuarioAutoridade> autoridades) {
		this.autoridades = autoridades;
	}
	
	public void setSupermercado(Supermercado supermercado) {
		this.supermercado = supermercado;
	}
	
	public Supermercado getSupermercado() {
		return supermercado;
	}

}
