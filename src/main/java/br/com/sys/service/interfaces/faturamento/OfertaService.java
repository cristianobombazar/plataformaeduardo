package br.com.sys.service.interfaces.faturamento;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.model.faturamento.Oferta;
import br.com.sys.service.interfaces.CrudService;

@Service
public interface OfertaService extends CrudService<Oferta, Integer> {

	List<Oferta> findAll(Supermercado supermercado) throws Exception;
	void deletarItem(Integer id) throws Exception;
	Oferta findOferta(Supermercado supermercado) throws Exception;
	Oferta findOferta(Long qrCode) throws Exception;
	
}
