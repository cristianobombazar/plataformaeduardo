package br.com.sys.service.interfaces.cadastro;

import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Bairro;
import br.com.sys.service.interfaces.CrudService;

@Service
public interface BairroService extends CrudService<Bairro, Integer>  {

}
