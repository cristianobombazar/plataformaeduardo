package br.com.sys.service.interfaces.seguranca;

import br.com.sys.model.seguranca.Usuario;
import br.com.sys.service.interfaces.CrudService;

public interface UsuarioService extends CrudService<Usuario, Integer>{

	Usuario getUsuario(String loginOrEmail); 
	
}
