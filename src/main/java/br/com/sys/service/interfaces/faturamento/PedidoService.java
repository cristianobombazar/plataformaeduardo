package br.com.sys.service.interfaces.faturamento;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;

import br.com.sys.dto.filtro.Filtro;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.model.faturamento.Pedido;
import br.com.sys.service.interfaces.CrudService;
import net.sf.jasperreports.engine.JasperPrint;

@Service
public interface PedidoService extends CrudService<Pedido, Integer> {

	List<Pedido> findAll(Supermercado supermercado) throws Exception;
	void deletarItem(Integer id) throws Exception;
	JasperPrint relatorioPedido(Filtro filtro) throws Exception;
	
}
