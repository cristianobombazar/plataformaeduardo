package br.com.sys.service.interfaces.util;

import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Bairro;
import br.com.sys.model.cadastro.Cep;
import br.com.sys.model.cadastro.Municipio;

@Service
public interface UtilService {
	
	
	Municipio findMunicipioByNome(String nome) throws Exception;
	Bairro findBairroByNome(String nome) throws Exception;
	Cep findCepByCodigo(Integer codigo) throws Exception;
	
	

}
