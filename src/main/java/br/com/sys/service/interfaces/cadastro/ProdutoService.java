package br.com.sys.service.interfaces.cadastro;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import br.com.sys.model.cadastro.Produto;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.service.interfaces.CrudService;
import net.sf.jasperreports.engine.JasperPrint;

@Service
public interface ProdutoService extends CrudService<Produto, Integer> {
	
	String generateBase64QrCode(Object...args) throws Exception;
	List<Produto> findAll(Supermercado supermercado) throws Exception;
	JasperPrint relatorioProdutoVendido(Date dataInicial, Date dataFinal, Supermercado supermercado) throws Exception;
}
