package br.com.sys.service.interfaces;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Order;

public interface CrudService<T, ID extends Serializable> {

	Page<T> findAll(Integer pagina, Integer tamanho) throws Exception;

	Page<T> findAll(Integer pagina, Integer tamanho, List<Order> orders) throws Exception;

	List<T> findAll() throws Exception;
	
	T findOne(ID id) throws Exception;

	void delete(ID id) throws Exception;

	T save(T t) throws Exception;
	
	List<T> findAll(Example<T> example) throws Exception;
}
