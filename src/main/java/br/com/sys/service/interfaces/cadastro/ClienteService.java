package br.com.sys.service.interfaces.cadastro;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Cliente;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.service.interfaces.CrudService;

@Service
public interface ClienteService extends CrudService<Cliente, Integer> {

	List<Cliente> findAll(Supermercado supermercado) throws Exception;
	
}
