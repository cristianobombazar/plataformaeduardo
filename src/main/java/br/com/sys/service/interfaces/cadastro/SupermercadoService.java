package br.com.sys.service.interfaces.cadastro;

import org.springframework.stereotype.Service;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.service.interfaces.CrudService;

@Service
public interface SupermercadoService extends CrudService<Supermercado, Integer> {

}
