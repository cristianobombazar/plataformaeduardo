package br.com.sys.service.interfaces.cadastro;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.FormaPagamento;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.service.interfaces.CrudService;

@Service
public interface FormaPagamentoService extends CrudService<FormaPagamento, Integer> {

	List<FormaPagamento> findAll(Supermercado supermercado) throws Exception;
}
