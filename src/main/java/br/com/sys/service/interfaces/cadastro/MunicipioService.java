package br.com.sys.service.interfaces.cadastro;

import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Municipio;
import br.com.sys.service.interfaces.CrudService;

@Service
public interface MunicipioService extends CrudService<Municipio, Integer> {

}
