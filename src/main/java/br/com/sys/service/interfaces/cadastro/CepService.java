package br.com.sys.service.interfaces.cadastro;

import br.com.sys.model.cadastro.Cep;
import br.com.sys.service.interfaces.CrudService;

public interface CepService  extends CrudService<Cep, Integer>{

}
