package br.com.sys.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import br.com.sys.model.seguranca.Usuario;
import br.com.sys.util.AutenticacaoUsuario;
import br.com.sys.util.TokenHandler;

@Service
public class TokenAuthenticationService {

	private static final String AUTH_HEADER_NAME = "Authorization";

	private final TokenHandler tokenHandler;

	public TokenAuthenticationService() {
		byte[] key = Base64Utils.decodeFromString(
				"9SyECk96oDsTmXfogIieDI0cD/8FpnojlYSUJT5U9I/FGVmBz5oskmjOR8cbXTvoPjX+Pq/T/b1PqpHX0lYm0oCBjXWICA==");
		tokenHandler = new TokenHandler(key);
	}

	public void addAutenticacao(HttpServletResponse response, AutenticacaoUsuario autenticacao) {
		final Usuario usuario = autenticacao.getPrincipal();
		response.addHeader(AUTH_HEADER_NAME, tokenHandler.createTokenForUser(usuario));
	}

	public Authentication getAuthentication(HttpServletRequest request) {
		final String token = request.getHeader(AUTH_HEADER_NAME);
		if (token != null) {
			final Usuario usuario = tokenHandler.parseUserFromToken(token);
			if (usuario != null) {
				return new AutenticacaoUsuario(usuario);
			}
		}
		return null;
	}
}