package br.com.sys.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.sys.model.seguranca.Usuario;
import br.com.sys.repository.seguranca.UsuarioRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioRepository usuarioDao;

	// private final AccountStatusUserDetailsChecker detailsChecker = new
	// AccountStatusUserDetailsChecker();
	@Override
	public Usuario loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioDao.getUsuario(username, username);
		if (usuario == null) {
			throw new UsernameNotFoundException("Usu�rio n�o encontrado");
		}
		// detailsChecker.check(usuario);
		return usuario;
	}

}
