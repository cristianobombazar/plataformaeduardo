package br.com.sys.service.impl.faturamento;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.sys.dto.RelatorioPedidoDTO;
import br.com.sys.dto.filtro.Filtro;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.model.faturamento.Pedido;
import br.com.sys.repository.faturamento.PedidoRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.faturamento.PedidoService;
import br.com.sys.util.GeradorRelatorio;
import net.sf.jasperreports.engine.JasperPrint;

@Service
public class PedidoServiceImpl extends DefaultCrudService<Pedido, Integer> implements PedidoService {

	private PedidoRepository repository;
	
	@Autowired
	private EntityManager em;
	
	@Autowired
	public PedidoServiceImpl(PedidoRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public List<Pedido> findAll(Supermercado supermercado) throws Exception {
		return repository.findAll(supermercado);
	}

	@Override
	@Transactional
	public void deletarItem(Integer id) throws Exception {
		repository.deletarItem(id);	
	}
	
	private List<RelatorioPedidoDTO> carregaDadosRelatorio(Filtro filtro) throws Exception{
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pedido.id,");
		sql.append("       pedido.data,");
		sql.append("       cliente.nome,");
		sql.append("       (SELECT sum(item.quantidade * item.valor) FROM faturamento.pedido_item item WHERE pedido.id = item.pedido_id) as total,");
		sql.append("        pedido.situacao");
		sql.append("  FROM faturamento.pedido pedido");
		sql.append("  JOIN cadastro.cliente ON (pedido.cliente_id = cliente.id)");
		sql.append(" WHERE pedido.data BETWEEN :dataInicial AND :dataFinal");
		sql.append("   AND pedido.supermercado_id = :supermercadoID");
		if (filtro.getStatus() != null) {
			sql.append("   AND pedido.situacao = :status");
		}
		if (filtro.getProduto() != null) {
			sql.append(" AND EXISTS (SELECT true FROM faturamento.pedido_item item WHERE item.pedido_id = pedido.id AND item.produto_id = :produtoID)");
		}
		if (filtro.getCliente() != null) {
			sql.append(" AND pedido.cliente_id = :clienteID");
		}
		Query query = em.createNativeQuery(sql.toString());
		query.setParameter("dataInicial", filtro.getDataInicial());
		query.setParameter("dataFinal", filtro.getDataFinal());
		query.setParameter("supermercadoID", filtro.getSupermercado().getId());
		if (filtro.getStatus() != null) {
			query.setParameter("status", filtro.getStatus());
		}
		if (filtro.getProduto() != null) {
			query.setParameter("produtoID", filtro.getProduto().getId());
		}
		if (filtro.getCliente() != null) {
			query.setParameter("clienteID", filtro.getCliente().getId());
		}
		List<Object[]> result = query.getResultList();
		if (result != null && !result.isEmpty()) {
			List<RelatorioPedidoDTO> lista = new ArrayList<>();
			for(Object[] dados : result) {
				Integer    pedidoID     = (Integer) dados[0];
				Date       pedidoData   = (Date)    dados[1];
				String     clienteNome  = (String)  dados[2];
				BigDecimal pedidoTotal  = (BigDecimal) dados[3];
				Integer    pedidoStatus = (Integer) dados[4];
				
				RelatorioPedidoDTO rel = new RelatorioPedidoDTO();
				rel.setPedidoID(pedidoID);
				rel.setPedidoData(pedidoData);
				rel.setClienteNome(clienteNome);
				rel.setPedidoTotal(pedidoTotal);
				if (pedidoStatus == Pedido.STATUS_PEDIDO_CANCELADO) {
					rel.setPedidoStatus("CANCELADO");
				}else if (pedidoStatus == Pedido.STATUS_PEDIDO_FATURADO) {
					rel.setPedidoStatus("FATURADO");
				}else if (pedidoStatus == Pedido.STATUS_PEDIDO_LIBERADO) {
					rel.setPedidoStatus("LIBERADO");
				}else {
					rel.setPedidoStatus("Pendente");
				}
				lista.add(rel);
			}
			return lista;
		}
		return null;
	}

	@Override
	public JasperPrint relatorioPedido(Filtro filtro) throws Exception{
		List<RelatorioPedidoDTO> rel = carregaDadosRelatorio(filtro);
		if (rel != null && !rel.isEmpty()) {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("DATA_INICIAL", filtro.getDataInicial());
			parameters.put("DATA_FINAL", filtro.getDataFinal());
			GeradorRelatorio geradorRelatorio = new GeradorRelatorio(rel);
			return geradorRelatorio.gerarRelatorio("relatorioPedido", parameters);
		}
		return null;
	}

}
