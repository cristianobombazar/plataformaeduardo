package br.com.sys.service.impl.util;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Bairro;
import br.com.sys.repository.cadastro.BairroRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.cadastro.BairroService;

@Service
public class BairroServiceImpl extends DefaultCrudService<Bairro, Integer> implements BairroService {

	@Autowired
	public BairroServiceImpl(BairroRepository bairroDao) {
		super(bairroDao);
	}
	
}
