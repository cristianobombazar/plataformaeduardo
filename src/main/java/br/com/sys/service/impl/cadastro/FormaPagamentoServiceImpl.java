package br.com.sys.service.impl.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.FormaPagamento;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.repository.cadastro.FormaPagamentoRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.cadastro.FormaPagamentoService;

@Service
public class FormaPagamentoServiceImpl extends DefaultCrudService<FormaPagamento, Integer> implements FormaPagamentoService {

	private FormaPagamentoRepository respository;
	
	@Autowired
	public FormaPagamentoServiceImpl(FormaPagamentoRepository respository) {
		super(respository);
		this.respository = respository;
	}

	@Override
	public List<FormaPagamento> findAll(Supermercado supermercado) throws Exception {
		return respository.findAll(supermercado);
	}

	
}
