package br.com.sys.service.impl.seguranca;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import br.com.sys.model.seguranca.Usuario;
import br.com.sys.repository.seguranca.UsuarioRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.TransactionalForException;
import br.com.sys.service.interfaces.seguranca.UsuarioService;

@Service
@TransactionalForException
public class UsuarioServiceImpl extends DefaultCrudService<Usuario, Integer> implements UsuarioService {

	private UsuarioRepository usuarioDao;

	@Autowired
	public UsuarioServiceImpl(UsuarioRepository usuarioDao) {
		super(usuarioDao);
		this.usuarioDao = usuarioDao;
	}

	@Override
	public Usuario save(Usuario usuario) throws Exception {
		String novaSenha = BCrypt.hashpw(usuario.getSenha(), BCrypt.gensalt());
		usuario.setSenha(novaSenha);
		return usuarioDao.save(usuario);
	}

	@Override
	public Usuario getUsuario(String loginOrEmail) {
		return usuarioDao.getUsuario(loginOrEmail, loginOrEmail);
	}

}
