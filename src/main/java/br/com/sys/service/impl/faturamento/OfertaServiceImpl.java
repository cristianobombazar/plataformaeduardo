package br.com.sys.service.impl.faturamento;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.model.faturamento.Oferta;
import br.com.sys.repository.faturamento.OfertaRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.faturamento.OfertaService;

@Service
public class OfertaServiceImpl extends DefaultCrudService<Oferta, Integer> implements OfertaService {

	private OfertaRepository repository;
	
	@Autowired
	private EntityManager em;
	
	@Autowired
	public OfertaServiceImpl(OfertaRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public List<Oferta> findAll(Supermercado supermercado) throws Exception {
		return repository.findAll(supermercado);
	}

	@Override
	@Transactional
	public void deletarItem(Integer id) throws Exception {
		repository.deletarItem(id);
	}

	@Override
	public Oferta findOferta(Supermercado supermercado) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT oferta.id");
		sql.append("  FROM Oferta oferta");
		sql.append(" WHERE oferta.supermercado.id = :supermercadoID");
		sql.append("   AND cast(:dataOferta as date) BETWEEN oferta.dataInicio AND oferta.dataFim");
		TypedQuery<Integer> query = (TypedQuery<Integer>) em.createQuery(sql.toString(), Integer.class);
		query.setMaxResults(1);
		query.setParameter("supermercadoID", supermercado.getId());
		query.setParameter("dataOferta", new Date());
		Integer idOFerta = query.getSingleResult();
		return repository.findOne(idOFerta);
	}

	@Override
	public Oferta findOferta(Long qrCode) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT oferta");
		sql.append("  FROM Oferta oferta");
		sql.append(" WHERE cast(:dataOferta as date) BETWEEN oferta.dataInicio AND oferta.dataFim");
		sql.append("   AND EXISTS (SELECT ofertaProduto.id");
		sql.append("                 FROM OfertaProduto ofertaProduto");
		sql.append("                 JOIN ofertaProduto.produto produto");
		sql.append("                WHERE produto.generatedValueQrCodeEncoder = :qrCode");
		sql.append("                  AND ofertaProduto.oferta = oferta)");
		TypedQuery<Oferta> query = (TypedQuery<Oferta>) em.createQuery(sql.toString(), Oferta.class);
		query.setMaxResults(1);
		query.setParameter("dataOferta", new Date());
		query.setParameter("qrCode", qrCode);
		return query.getSingleResult();
	}

	
}
