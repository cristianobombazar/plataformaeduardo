package br.com.sys.service.impl.cadastro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Bairro;
import br.com.sys.model.cadastro.Cep;
import br.com.sys.model.cadastro.Cliente;
import br.com.sys.model.cadastro.ClienteEndereco;
import br.com.sys.model.cadastro.Municipio;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.model.seguranca.Usuario;
import br.com.sys.repository.cadastro.ClienteRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.cadastro.BairroService;
import br.com.sys.service.interfaces.cadastro.CepService;
import br.com.sys.service.interfaces.cadastro.ClienteService;
import br.com.sys.service.interfaces.cadastro.MunicipioService;
import br.com.sys.service.interfaces.util.UtilService;

@Service
public class ClienteServiceImpl extends DefaultCrudService<Cliente, Integer> implements ClienteService {

	@Autowired
	private UtilService utilService;
	@Autowired
	private MunicipioService municipioService;
	@Autowired
	private BairroService bairroService;
	@Autowired
	private CepService cepService;

	private ClienteRepository repository;

	@Autowired
	public ClienteServiceImpl(ClienteRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	public List<Cliente> findAll(Supermercado supermercado) throws Exception {
		return repository.findAll(supermercado);
	}

	@Override
	public Cliente save(Cliente cliente) throws Exception {
		Municipio municipio = utilService.findMunicipioByNome(cliente.getEndereco().getMunicipio().getNome());
		Bairro bairro = utilService.findBairroByNome(cliente.getEndereco().getBairro().getNome());
		Cep cep = utilService.findCepByCodigo(cliente.getEndereco().getCep().getCodigo());
		if (municipio == null) {
			cliente.getEndereco().getMunicipio().setId(null);
			municipio = municipioService.save(cliente.getEndereco().getMunicipio());
		}
		if (bairro == null) {
			cliente.getEndereco().getBairro().setId(null);
			bairro = bairroService.save(cliente.getEndereco().getBairro());
		}
		if (cep == null) {
			cliente.getEndereco().getCep().setId(null);
			cep = cepService.save(cliente.getEndereco().getCep());
		}
		ClienteEndereco endereco = cliente.getEndereco();
		endereco.setCliente(cliente);
		endereco.setBairro(bairro);
		endereco.setMunicipio(municipio);
		endereco.setCep(cep);
		cliente.setEndereco(endereco);
		if (cliente.getSupermercado() == null) {
			cliente.setSupermercado(((Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getSupermercado());
		}
		if (cliente.getCartoes() != null && !cliente.getCartoes().isEmpty()) {
			cliente.getCartoes().forEach(cartao -> {
				cartao.setCliente(cliente);
			});
		}
		return super.save(cliente);
	}

}
