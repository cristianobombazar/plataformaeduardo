package br.com.sys.service.impl.cadastro;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.zxing.EncodeHintType;

import br.com.sys.dto.RelatorioProdutoVendidoDTO;
import br.com.sys.model.cadastro.Produto;
import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.repository.cadastro.ProdutoRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.cadastro.ProdutoService;
import br.com.sys.util.GeradorRelatorio;
import br.com.sys.util.Util;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import net.sf.jasperreports.engine.JasperPrint;

@Service
public class ProdutoServiceImpl extends DefaultCrudService<Produto, Integer> implements ProdutoService {
	
	private ProdutoRepository repository;
	
	@Autowired
	private EntityManager em;
	
	@Autowired
	public ProdutoServiceImpl(ProdutoRepository repository) {
		super(repository);
		this.repository = repository;
	}
	

	@Override
	public List<Produto> findAll(Supermercado supermercado) throws Exception {
		return repository.findAll(supermercado);
	}
	
	@Override
	public String generateBase64QrCode(Object... args) throws Exception {
		String base64String = "data:image/png;base64,";
		String value = "";
		for (Object data : args) {
			value += data + ";";
		}
		File outFile = new File("produto_qrcode_generator.png");
		QRCode qrCode = QRCode.from(value);
		qrCode.withHint(EncodeHintType.MARGIN, 2);
		qrCode.withSize(270, 270);
		qrCode.to(ImageType.PNG);
		ByteArrayOutputStream outprio = qrCode.stream();
		
		FileOutputStream foutprio = new FileOutputStream(outFile);
		foutprio.write(outprio.toByteArray());
		foutprio.flush();
		foutprio.close();

		BufferedImage buffer = ImageIO.read(new File("produto_qrcode_generator.png"));
		String base64 = Util.encodeToString(buffer, "png");
		outFile.delete();
		return base64String+base64;
	}
	
	private List<RelatorioProdutoVendidoDTO> carregaDadosRelatorio(Date dataInicial, Date dataFinal, Supermercado supermercado) throws Exception{
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT produto.id as produtoid, produto.descricao as produtodescricao, sum(item.quantidade) as quantidade"); 
		sql.append("  FROM faturamento.pedido_item item"); 
		sql.append("  JOIN cadastro.produto ON (produto.id = item.produto_id)"); 
		sql.append(" WHERE EXISTS (SELECT true FROM faturamento.pedido WHERE item.pedido_id = pedido.id AND pedido.data BETWEEN :dataInicial AND :dataFinal AND pedido.supermercado_id = :supermercado)");		
		sql.append(" GROUP BY produto.id, produto.descricao");
		sql.append(" ORDER BY quantidade DESC");
		Query query = (TypedQuery<RelatorioProdutoVendidoDTO>) em.createNativeQuery(sql.toString());
		query.setParameter("dataInicial", dataInicial);
		query.setParameter("dataFinal", dataFinal);
		query.setParameter("supermercado", supermercado.getId());
		List<Object[]> result = query.getResultList();
		List<RelatorioProdutoVendidoDTO> lista = null;
		if (result != null && !result.isEmpty()) {
			lista = new ArrayList<>();
			for (Object[] dados : result) {
				Integer    produtoID        = (Integer) dados[0];
				String     produtoDescricao = (String) dados[1];
				BigDecimal quantidade       = (BigDecimal) dados[2];
				
				RelatorioProdutoVendidoDTO dto = new RelatorioProdutoVendidoDTO();
				dto.setQuantidade(quantidade);
				dto.setProdutoID(produtoID);
				dto.setProdutoDescricao(produtoDescricao);
				lista.add(dto);
			}
		}
		return lista;
	}


	@Override
	public JasperPrint relatorioProdutoVendido(Date dataInicial, Date dataFinal, Supermercado supermercado) throws Exception {
		List<RelatorioProdutoVendidoDTO> rel = carregaDadosRelatorio(dataInicial, dataFinal, supermercado);
		if (rel != null && !rel.isEmpty()) {
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("DATA_INICIAL", dataInicial);
			parameters.put("DATA_FINAL", dataFinal);
			GeradorRelatorio gerador = new GeradorRelatorio(rel);
			return gerador.gerarRelatorio("relatorioProdutoVendido", parameters);
		}
		return null;
	}


}
