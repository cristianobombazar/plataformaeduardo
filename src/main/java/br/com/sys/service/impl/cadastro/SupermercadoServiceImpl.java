package br.com.sys.service.impl.cadastro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Supermercado;
import br.com.sys.repository.cadastro.SupermercadoRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.cadastro.SupermercadoService;

@Service
public class SupermercadoServiceImpl extends DefaultCrudService<Supermercado, Integer> implements SupermercadoService {

	@Autowired
	public SupermercadoServiceImpl(SupermercadoRepository supermercadoDao) {
		super(supermercadoDao);
	}

}
