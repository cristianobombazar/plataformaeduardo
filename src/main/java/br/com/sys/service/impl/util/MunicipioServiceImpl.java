package br.com.sys.service.impl.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Municipio;
import br.com.sys.repository.cadastro.MunicipioRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.cadastro.MunicipioService;

@Service
public class MunicipioServiceImpl extends DefaultCrudService<Municipio, Integer> implements MunicipioService {

	@Autowired
	public MunicipioServiceImpl(MunicipioRepository municipioDao) {
		super(municipioDao);
	}

}
