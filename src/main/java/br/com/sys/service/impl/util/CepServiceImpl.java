package br.com.sys.service.impl.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Cep;
import br.com.sys.repository.cadastro.CepRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.cadastro.CepService;

@Service
public class CepServiceImpl extends DefaultCrudService<Cep, Integer> implements CepService {

	@Autowired
	public CepServiceImpl(CepRepository cepDao) {
		super(cepDao);
	}
	
}
