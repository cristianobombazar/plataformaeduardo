package br.com.sys.service.impl.cadastro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.sys.model.cadastro.ClienteCartao;
import br.com.sys.repository.cadastro.ClienteCartaoRepository;
import br.com.sys.service.DefaultCrudService;
import br.com.sys.service.interfaces.cadastro.ClienteCartaoService;

@Service
public class ClienteCartaoServiceImpl extends DefaultCrudService<ClienteCartao, Integer> implements ClienteCartaoService {

	private ClienteCartaoRepository repository;
	
	@Autowired
	public ClienteCartaoServiceImpl(ClienteCartaoRepository repository) {
		super(repository);
		this.repository = repository;
	}

	@Override
	@Transactional
	public void deletarCartao(Integer id) throws Exception {
		repository.deletarCartao(id);
	}
	
}
