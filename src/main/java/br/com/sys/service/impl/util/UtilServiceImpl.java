package br.com.sys.service.impl.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sys.model.cadastro.Bairro;
import br.com.sys.model.cadastro.Cep;
import br.com.sys.model.cadastro.Municipio;
import br.com.sys.service.interfaces.util.UtilService;
import br.com.sys.util.Util;

@Service
@SuppressWarnings("unchecked")
public class UtilServiceImpl implements UtilService {
	
	@Autowired
	private EntityManager em;

	@Override
	public Municipio findMunicipioByNome(String nome) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM cadastro.municipio WHERE UPPER(nome) = '"+Util.normalizeString(nome)+"'");
		TypedQuery<Municipio> query = (TypedQuery<Municipio>) em.createNativeQuery(sql.toString(), Municipio.class);
		List<Municipio> lista = query.getResultList();
		return lista != null && !lista.isEmpty() ? lista.get(0) : null;
	}

	@Override
	public Bairro findBairroByNome(String nome) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM cadastro.bairro WHERE UPPER(nome) = '"+Util.normalizeString(nome)+"'");
		TypedQuery<Bairro> query = (TypedQuery<Bairro>) em.createNativeQuery(sql.toString(), Bairro.class);
		List<Bairro> lista = query.getResultList();
		return lista != null && !lista.isEmpty() ? lista.get(0) : null;
	}

	@Override
	public Cep findCepByCodigo(Integer codigo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM cadastro.cep WHERE codigo = "+codigo+"");		
		TypedQuery<Cep> query = (TypedQuery<Cep>) em.createNativeQuery(sql.toString(), Cep.class);
		List<Cep> lista = query.getResultList();
		return lista != null && !lista.isEmpty() ? lista.get(0) : null;
	}
	
}
