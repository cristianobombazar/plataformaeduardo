package br.com.sys.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.sys.service.interfaces.CrudService;

public abstract class DefaultCrudService<T, ID extends Serializable> implements CrudService<T, ID> {
	
	private final JpaRepository<T, ID> dao;
	
	public DefaultCrudService(JpaRepository<T, ID> dao){
		this.dao = dao;
	}

	@Override
	public Page<T> findAll(Integer pagina, Integer tamanho) throws Exception {
		List<Order> orders = new ArrayList<>();
		orders.add(getDefaultOrder());
		return findAll(pagina, tamanho, orders);
	}

	@Override
	public Page<T> findAll(Integer pagina, Integer tamanho, List<Order> orders) throws Exception {
		return dao.findAll(new PageRequest(pagina, tamanho, new Sort(orders)));
	}

	@Override
	public List<T> findAll(Example<T> example) throws Exception {
		return dao.findAll(example);
	}

	@Override
	public List<T> findAll() throws Exception {
		return dao.findAll();
	}

	@Override
	public T findOne(ID id) throws Exception {
		return dao.findOne(id);
	}

	@Override
	public void delete(ID id) throws Exception {
		dao.delete(id);
	}

	@Override
	public T save(T t) throws Exception {
		return dao.save(t);
	}
	
	public Order getDefaultOrder(){
		return new Order(Sort.Direction.ASC, "id");
	}

}
